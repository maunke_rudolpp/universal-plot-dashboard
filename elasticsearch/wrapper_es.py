# =========
# LIBRARIES
# =========
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import pandas as pd
import json

# =========
# WRAPPER
# =========


class WrapperES:
    """Elasticsearch wrapper"""

    def __init__(self, host, port):
        self.es = Elasticsearch([{'host': host, 'port': port}], timeout=1000)
        self.idx_desc = 'idx_description'
        self.idx_data = 'idx_data_'
        self.doc_desc = 'doc_description'
        self.config_desc = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            }
        }
        self.conifg_data = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0,
                "codec": 'best_compression',
            },
            "mappings": {
            }
        }

    def create_description_index(self, df_desc):
        """Upload or update column description"""
        if not self.es.indices.exists(index=self.idx_desc):
            self.es.indices.create(
                index=self.idx_desc, body=self.config_desc)
        for idx, row in df_desc.iterrows():
            if "id" in row and row['id'] != '':
                self.es.index(
                    index=self.idx_desc,
                    id=row['id'],
                    doc_type=self.doc_desc,
                    body=row.to_json())

    def create_data_index(self, id_data, df_data):
        """Create new index with one doctype"""
        index = self.idx_data + id_data
        # Recreate index
        if self.es.indices.exists(index=index):
            self.es.indices.delete(index=index)
        self.es.indices.create(index=index, body=self.conifg_data)
        # Get column types for mapping
        res = self.es.search(index=self.idx_desc, body={
            "query": {"match_all": {}}, "size": 1000})['hits']['hits']
        df_desc = pd.DataFrame([x['_source'] for x in res])
        # Define mappings (can be adjusted if needed, e.g. for different date formate)
        dict_type = {
            "geoshape": {
                "type": "geo_shape",
                "tree": "quadtree",
                "precision": "1000m",
                "distance_error_pct": 0.01,
                "ignore_malformed": True
            },
            "date": {
                "type": "date",
        #        "format": "yyyy-MM-dd'T'HH:mm:ss"
            },
            "double": {
                "type": "double",
            },
            "integer": {
                "type": "integer",
            },
            "keyword": {
                "type": "keyword",
            }
        }
        # Build mapping
        mapping = {'properties': {}}
        for column in df_data.columns:
            if column in list(df_desc['id']):
                col_type = list(
                    df_desc.loc[column == df_desc['id']]['type'])[0]
                mapping['properties'][column] = dict_type[col_type]
            else:
                print(
                    column + " not in column description, column wont be shown in dashboard")
        # Upload mapping to index
        self.es.indices.put_mapping(
            index=index,
            doc_type=id_data,
            body=json.dumps(mapping)
        )

    def upload_data(self, id_data, df_data):
        """Upload or update data"""
        index = self.idx_data + id_data
        # generate actions with yield (safes memory)

        def generate_actions(data):
            for idx, row in data.iterrows():
                yield {
                    '_op_type': 'index',
                    '_index': index,
                    '_type': id_data,
                    '_id': idx,
                    '_source': row.to_json()
                }
        # do bulk operation in parallel with 500 chunks
        for success, info in helpers.parallel_bulk(
                self.es,
                generate_actions(df_data),
                thread_count=4,
                chunk_size=500):
            pass

    def query(self, idx, query):
        """Query interface for es"""
        res = self.es.search(index=idx, body=query)
        return res

    def mquery(self, requests):
        """Multi-Query interface for es"""
        res = self.es.msearch(body=requests)
        return res

    def squery(self, idx, query, num_pages):
        """Scroll-Query interface for es"""
        # make first search request
        page = self.es.search(index=idx, body=query, scroll='2m')
        sid = page['_scroll_id']
        scroll_size = page['hits']['total']
        res = page['hits']['hits']
        count = 0
        # get number of following pages specified in num_pages
        # stop if returned pages are either empty or count has been reached
        while (scroll_size > 0 and count != num_pages):
            page = self.es.scroll(scroll_id=sid, scroll='2m')
            res += page['hits']['hits']
            sid = page['_scroll_id']
            scroll_size = len(page['hits']['hits'])
            count += 1
        return res
