// =============================================================================
// EVENTS
// =============================================================================
$.fn.extend({
  /** progress bar handler
   * arg:
   * - true: add loading progress
   * - false: progress done
   **/
  event: {
    progress: function(arg) {
      var prog_bar = $(".progress div");
      var progs = global.progress.count;
      // count active progresses
      progs = arg ? progs + 1 : Math.max(progs - 1, 0);
      // active progress
      var new_class = "indeterminate";
      // inactive progress
      var old_class = "determinate";
      // swap class when all progresses done
      if (progs == 0) {
        [new_class, old_class] = [old_class, new_class];
      }
      prog_bar.removeClass(old_class).addClass(new_class);
      // update global progress count
      global.progress.count = progs;
    },
    konami: function() {
      // a key map of allowed keys
      var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        65: 'a',
        66: 'b'
      };
      // the 'official' Konami Code sequence
      var konamiCode = ['up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'b', 'a'];
      // a variable to remember the 'position' the user has reached so far.
      var konamiCodePosition = 0;
      var konami_active = false;
      // add keydown event listener
      document.addEventListener('keydown', function(e) {
        // get the value of the key code from the key map
        var key = allowedKeys[e.keyCode];
        // get the value of the required key from the konami code
        var requiredKey = konamiCode[konamiCodePosition];
        // compare the key with the required key
        if (key == requiredKey) {
          // move to the next key in the konami code sequence
          konamiCodePosition++;
          // if the last key is reached, activate cheats
          if (konamiCodePosition == konamiCode.length && !konami_active) {
            konami_active = true;
            konami_content();
            konamiCodePosition = 0;
          }
        } else {
          konamiCodePosition = 0;
        }
      });

      var video_pos = 0;
      // ;autoplay=1
      var yt_links = [
        "https://www.youtube-nocookie.com/embed/KaqC5FnvAEc?rel=0&amp;controls=0&amp;showinfo=0&amp;start=14",
        "https://www.youtube-nocookie.com/embed/YMZ6FvpvleA?rel=0&amp;controls=0&amp;showinfo=0&amp;start=148",
        "https://www.youtube-nocookie.com/embed/LUEDVMOMY24?rel=0&amp;controls=0&amp;showinfo=0&amp;start=43",
        "https://www.youtube-nocookie.com/embed/jTmXHvGZiSY?rel=0&amp;controls=0&amp;showinfo=0&amp;start=5",
        "https://www.youtube-nocookie.com/embed/lfvRI_Air4E?rel=0&amp;controls=0&amp;showinfo=0&amp;start=34",
        "https://www.youtube-nocookie.com/embed/US7GgY5a4Ug?rel=0&amp;controls=0&amp;showinfo=0&amp;start=66",
        "https://www.youtube-nocookie.com/embed/akK4mzEZVRU?rel=0&amp;controls=0&amp;showinfo=0&amp;start=23",
        "https://www.youtube-nocookie.com/embed/O1WpE5ntqbQ?rel=0&amp;controls=0&amp;showinfo=0&amp;start=178",
        "https://www.youtube-nocookie.com/embed/RHevGGYuAsw?rel=0&amp;controls=0&amp;showinfo=0;start=29",
      ];

      function konami_content() {
        // konami stuff
        $("#konami").remove();
        var content = '<iframe id="konami" style="z-index:9999;position:absolute;width:100%;height:100%;" src="' + yt_links[video_pos % yt_links.length] + ';autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        video_pos += 1;
        $("body").append(content).delay(12500).queue(function(next) {
          $("#konami").off().remove();
          konami_active = false;
          next();
        });
      }
    },
    server_status: function() {
      var color_status = {
        online: "light-green-text text-lighten-1",
        offline: "red-text text-lighten-2"
      };

      function update_server_status() {
        $.ajax({
          type: 'GET',
          url: "/_server_status",
          dataType: 'json',
          contentType: 'application/json',
          timeout: 10000,
          cache: false
        }).done(function(e) {
          var el = $("#footer_server_status");
          el.html(e.message);
          if (e.message == "online") {
            el.removeClass(color_status.offline).addClass(color_status.online);
          } else {
            el.removeClass(color_status.online).addClass(color_status.offline);
          }
        }).fail(function() {
          $("#footer_server_status").html("offline").removeClass(color_status.online).addClass(color_status.offline);
        });
      }
      setInterval(update_server_status, 60000);
      update_server_status();
    },
    total_number_datasets: function() {
      $.ajax({
        type: 'GET',
        url: "/_total_number_datasets",
        dataType: 'json',
        contentType: 'application/json',
        timeout: 10000,
        cache: false
      }).done(function(e) {
        var el = $("#footer_datasets_total");
        el.html(e.total_number_datasets);
      }).fail(function() {
        var el = $("#footer_datasets_total");
        el.html("NaN");
      });
    },
    dashboard_number_datasets: function() {
      $.ajax({
        type: 'POST',
        url: "/_dashboard_number_datasets",
        data: JSON.stringify({
          "selected_dimensions": global.dashboard.selected_dimensions,
          "filter_options": global.dashboard.filter_options
        }),
        dataType: 'json',
        contentType: 'application/json',
        timeout: global.config.ajax_timeout
      }).done(function(e) {
        var el = $("#footer_datasets_dashboard");
        el.html(e.dashboard_number_datasets);
      }).fail(function() {
        var el = $("#footer_datasets_dashboard");
        el.html("NaN");
      });
    }
  }
});