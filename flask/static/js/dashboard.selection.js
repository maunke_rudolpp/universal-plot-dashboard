// =============================================================================
// LOAD PREDEFINED DASHBOARDS INTO INDEX.HTML
// =============================================================================
$(document).ready(function() {
  // list of predefined dashboard
  // properties: title, description, url_data, img name.format
  var dashboard_selections = [{
    title: "Predefined Dashboard",
    description: "This dashboard is saved via URL in flask/../dashboard.selection.js",
    url_data: "?data=N4IgzgpgNhDGAuEAmB9JBLAthAdmdA9niAFwDaIAwgE4QCGiSABACIMQgA0VBmADlDroc8JgBUAnnw7cAMgVgNCOLiABCBagQCuAcwAWIALrcAZuiiJqaLLnxEwpMiZDnLEawT7xlj8qHQkUh5+QWFRSWlVeCkOEhAAawgJAHdNIO5YInNdUlAk1PS-CjZMOl1kcVoOIwBfWpdBCR14FC8fBydQH3gYYNLyyrFqx24fbDB4On5SAEYAVgAWACZ5gDYAdnmAZgAOAE5Z-e4MbDxfJypadmY2RFUNLT1DF3blYIAJengyvlUsnA5PIgHDaTAoMCwLRQKB+AAM9U43XQvTiIAA4hACF8GL9orZJtM-iQFit1ltFvN1rsTrZzp1yCB5IoOipXt53vFMdjvnjMtl0LkSKBQeDIdDYaQEQ0xii+vEPtpqNR0IocBAmABlOg4JASEC1IA",
    img: "dashboard.jpg"
  }];
  // append individual dashboard cards to modal
  for (var i in dashboard_selections) {
    var dash = dashboard_selections[i];
    var title = dash.title;
    var description = dash.description;
    var url_data = dash.url_data;
    var img = dash.img;
    var dash_card = '\
            <div class="col s6">\
                <div class="card grey col s12 lighten-5">\
                  <div class="card-image waves-effect waves-block waves-light">\
                    <img class="activator" src="../static/img/dashboards/' + img + '">\
                  </div>\
                  <div class="card-content">\
                    <span class="card-title blue-grey-text text-darken-1">' + title + '</span>\
                    <a href=' + url_data + '\
                      style="margin-bottom:5px;margin-right:-25px;margin-left:10px;" class="right btn-small waves-effect waves-darken blue-grey darken-1"><i class="material-icons left">play_arrow</i>Plot Dashboard</a>\
                    <a href="#" style="margin-bottom:5px;" class="right btn-small btn-flat activator waves-effect waves-darken grey lighten-5 blue-grey-text text-lighten-3"><i class="material-icons left">description</i>Description</a>\
                  </div>\
                  <div class="card-reveal">\
                    <span class="card-title blue-grey-text text-darken-1">Description<i class="material-icons right">close</i></span>\
                    <p class="blue-grey-text text-lighten-3">' + description + '</p>\
                  </div>\
                </div>\
            </div>';
    // select dashboard mdoal content
    $("#modal-choose-dashboard .modal-content .row").append(dash_card);
  }

});