# Universal Plot Dashboard
## Getting Started

### Prerequisites

Get the prerequisites from python using pip
```
pip install -r requirements.txt
```

Download elasticsearch (version 6.3) from https://www.elastic.co/downloads/elasticsearch

### Installing

Setup the configuration file config.json and either run the flask app like this
```
python3 flask/app.py
```
or (better) get it run on an Apache server using with mod_wsgi: http://flask.pocoo.org/docs/0.12/deploying/mod_wsgi/
## Example

- get the example dataset from here: https://data.cityofnewyork.us/Social-Services/sandyrelated/fs5z-tpv4 (export it as csv and save it to the example folder)
- run the script example/upload_data.py

### Select Dimensions
<img src="README/choose_plot_filter_dim.jpg">

### Filter range of selected dimension
<img src="README/filter_dim.jpg">

### Geofilter of geographic dimension
<img src="README/geofilter.jpg">

### Time interval filter
<img src="README/timefilter.jpg">

### Custom built dashboard
<img src="README/dashboard.jpg">

## Built With

* [Elasticsearch](https://www.elastic.co/) - Elasticsearch is a highly scalable open-source full-text search and analytics engine used as data storage
* [Flask](http://flask.pocoo.org/) - Flask is a microframework for Python used as backend
* [JQuery](https://api.jquery.com/) - JavaScript library used for interactive html page
* [Material](https://materializecss.com/) - Responsive front-end framework]
* [plotly.js](https://plot.ly/javascript/) - Library for charts and plots in js
* [leaflet.js](https://leafletjs.com/) - Library for  interactive maps in js
* [Leaflet IDW](https://github.com/JoranBeaufort/Leaflet.idw) -  Library for idw interpolation with leaflet maps


## License

This project is licensed under the MIT License
