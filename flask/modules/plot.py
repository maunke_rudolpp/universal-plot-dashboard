# ========================================
# Visualisierung, FLASK API PLOT
# Jonas Gütter, Paul Rudolph, Markus Unkel
# ========================================

# =========
# LIBRARIES
# =========

import json
import pandas as pd
import numpy as np
from shapely.wkt import loads
from shapely.geometry import mapping, shape, MultiPolygon

# =========
# API_PLOT
# =========


class API_PLOT():
    """Class for fetching the data for plots from elasticsearch"""

    def __init__(self, es, api, query, config):
        self.plot_map = {
            "Scatter": self.plot_scatter,
            "CategoricalMap": self.plot_polygon_cat_map,
            "PolygonMap": self.plot_polygon_map,
            "GeoHeatmap": self.plot_geo_heatmap,
            "Histogram": self.plot_histogram,
            "PieChart": self.plot_histogram,
            "Heatmap": self.plot_heatmap,
            "StackedBarChart": self.plot_heatmap,
            "GeoNumHeatmap": self.plot_geo_heatmap
        }
        self.es = es
        self.api = api
        self.config = config
        self.query = query

    def plot_scatter(self, dimensions, selected_dimensions, filter_options,
                     config):
        """Fetch data for scatter plot using scroll queries"""
        result = self.api.get_scroll_data(dimensions, selected_dimensions,
                                          filter_options, config)
        # join data
        data = dict()
        for hit in result:
            for label in dimensions:
                if label not in data:
                    data[label] = list()
                data[label].append(hit['_source'][label])
        return data

    def plot_histogram(self, dimensions, selected_dimensions, filter_options,
                       config):
        """Fetch data for different types of histogramms"""
        # histogram one dimension
        dimension = dimensions[0]
        # get type of dimension
        dimension_type = self.api.dimension_properties([dimension])[
            0]['_source']['type']
        if dimension_type == "date":
            return self.api._filter_date(dimension, selected_dimensions,
                                         filter_options, config)
        elif dimension_type in ["integer", "double"]:
            return self.api._filter_numeric(dimension, selected_dimensions,
                                            filter_options, config)
        elif dimension_type in ["keyword"]:
            return self.api._filter_categorial(dimension, selected_dimensions,
                                               filter_options,
                                               {"query_filtered_data": True})

    def plot_heatmap(self, dimensions, selected_dimensions, filter_options,
                     config):
        """Fetch data for different types of heatmaps(non geographic)"""
        # prepare general queries
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        query_filter = self.query.filter(False, None, filter_options)
        ranges_queries = list()
        ranges_keys = list()
        # get ranges
        for dim in dimensions:
            # get type of dim
            dimension_type = self.api.dimension_properties([dim])[
                0]['_source']['type']
            # date
            if dimension_type in ["date"]:
                query_range = {
                    "query": {
                        "bool": {
                            "must": query_dimensions,
                            "filter": {
                                "bool": {
                                    "must": query_filter
                                }
                            }
                        },
                    },
                    "size": 0,
                    "aggs": {
                        "max": {
                            "max": {
                                "field": dim,
                                "format": "epoch_millis"
                            }
                        },
                        "min": {
                            "min": {
                                "field": dim,
                                "format": "epoch_millis"
                            }
                        }
                    }
                }
                value_range = self.es.query(self.es.idx_data+"*",
                                            query_range)['aggregations']
                value_min = value_range['min']['value']
                value_max = value_range['max']['value']
                keys = np.linspace(value_min, value_max, num=100)

                keys = np.round(keys)

                queries = list()
                queries_key = list()
                for i in range(len(keys) - 1):
                    min_val = keys[i]
                    max_val = keys[i + 1]
                    option = {
                        "id": dim,
                        "config": {
                            "gte": min_val,
                            "lte": max_val
                        }
                    }
                    if i == len(keys) - 2:
                        option['config']['lte'] += 1

                    queries.append(self.query.filter_date(option))
                    queries_key.append(min_val)

            elif dimension_type in ["integer", "double"]:
                query_range = {
                    "query": {
                        "bool": {
                            "must": query_dimensions,
                            "filter": {
                                "bool": {
                                    "must": query_filter
                                }
                            }
                        },
                    },
                    "size": 0,
                    "aggs": {
                        "max": {
                            "max": {
                                "field": dim
                            }
                        },
                        "min": {
                            "min": {
                                "field": dim
                            }
                        }
                    }
                }
                value_range = self.es.query(self.es.idx_data+"*",
                                            query_range)['aggregations']
                value_min = value_range['min']['value']
                value_max = value_range['max']['value']

                keys = np.linspace(value_min, value_max, num=100)
                if dimension_type == "Integer":
                    keys = np.round(keys)

                queries = list()
                queries_key = list()
                for i in range(len(keys) - 1):
                    min_val = keys[i]
                    max_val = keys[i + 1]
                    option = {
                        "id": dim,
                        "config": {
                            "min": min_val,
                            "max": max_val
                        }
                    }
                    if i == len(keys) - 2:
                        option['config']['max'] += 1

                    queries.append(self.query.filter_numeric(option))
                    queries_key.append(min_val)

            elif dimension_type in [
                    "keyword",
            ]:
                query = {
                    "query": {
                        "bool": {
                            "must": query_dimensions,
                            "filter": {
                                "bool": {
                                    "must": query_filter
                                }
                            }
                        },
                    },
                    "size": 0,
                    "aggs": {
                        "values": {
                            "terms": {
                                "field": dim,
                                "size": 999
                            }
                        }
                    }
                }
                dimension_unique_values_count = self.es.query(
                    self.es.idx_data+"*",
                    query)['aggregations']['values']['buckets']
                keys = [el["key"] for el in dimension_unique_values_count]
                doc_counts = [
                    el["doc_count"] for el in dimension_unique_values_count
                ]
                table = pd.DataFrame(columns=["id", "count"])
                table["id"] = keys
                table["count"] = doc_counts
                table = table.sort_values(by='count', ascending=False)
                # generate queries
                queries = list()
                queries_key = list()
                for idx, key in enumerate(table.id.values):
                    if idx < 30:
                        option = {"id": dim, "config": {"keywords": [key]}}
                        queries.append(self.query.filter_categorial(option))
                        queries_key.append(key)

            ranges_queries.append(queries)
            ranges_keys.append(queries_key)

        # cartesian product of both query_list
        cart_queries = [[q0, q1] for q0 in ranges_queries[0]
                        for q1 in ranges_queries[1]]
        cart_keys = [[k0, k1] for k0 in ranges_keys[0]
                     for k1 in ranges_keys[1]]
        query_arr = list()
        for q in cart_queries:
            filter = q + query_filter
            query = {
                "query": {
                    "bool": {
                        "must": query_dimensions,
                        "filter": {
                            "bool": {
                                "must": filter
                            }
                        }
                    },
                },
                'size': 0
            }
            query_arr.append({'index': self.es.idx_data+"*"})
            query_arr.append(query)
        request = ''
        for each in query_arr:
            request += '%s \n' % json.dumps(each)
        resp = self.es.mquery(query_arr)
        heat_counts = [r['hits']['total'] for r in resp['responses']]

        data = dict()
        data['counts'] = heat_counts
        data[dimensions[0]] = list()
        data[dimensions[1]] = list()
        for key_pair in cart_keys:
            data[dimensions[0]].append(key_pair[0])
            data[dimensions[1]].append(key_pair[1])

        data["heat_" + dimensions[0]] = ranges_keys[0]
        data["heat_" + dimensions[1]] = ranges_keys[1]
        data["heat_counts"] = np.reshape(
            np.array(heat_counts), (len(ranges_keys[1]), len(ranges_keys[0])),
            order="F")
        return data

    def plot_polygon_map(self, dimensions, selected_dimensions,
                         filter_options, config):
        """Fetch data for geographic polygon map (one color)"""
        result = self.api.get_scroll_data(dimensions, selected_dimensions,
                                          filter_options, config)
        data = list()
        for hit in result:
            data.append(mapping(loads(hit['_source'][dimensions[0]])))
        return data

    def plot_polygon_cat_map(self, dimensions, selected_dimensions,
                             filter_options, config):
        """Fetch data for geographic polygon map for two dimensions"""
        def map_poly(obj):
            """Maps as polygon as wkt to geojson"""
            try:
                x = mapping(loads(obj))
            except:
                x = np.nan
                print("Problem with" + str(obj))
            return x
        # Check if dimension is geographic and which should be considered as categorical
        snd_dim = dimensions[0]
        fst_dim = dimensions[1]
        snd_dim_type = ''
        for dim in dimensions:
            snd_dim = dim
            snd_dim_type = self.api.dimension_properties(
                [snd_dim])[0]['_source']['type']
            if snd_dim_type != 'geoshape':
                break
            fst_dim = snd_dim

        result = self.api.get_scroll_data(dimensions, selected_dimensions,
                                          filter_options, config)
        # build data table
        data = dict()
        for hit in result:
            for label in dimensions:
                if label not in data:
                    data[label] = list()
                data[label].append(hit['_source'][label])
        table = pd.DataFrame(data)
        table[fst_dim] = table[fst_dim].apply(lambda x: mapping(loads(x)))
        table[fst_dim] = table[fst_dim].dropna()
        # set categories in the data table to integers and create mapping from integers to categories
        categories = list()
        if snd_dim_type == 'keyword':
            # extract filter_dimensions
            filter_dimensions = [filter_opt['id']
                                 for filter_opt in filter_options]
            # dimensions and filter dimensions must exist
            query_dimensions = self.query.dimensions(
                selected_dimensions + filter_dimensions)
            # generate filter options '_' is dummy for a dimension which normally will be excluded
            # we don't have anything to exclude here
            query_filter = self.query.filter(False, '_', filter_options)
            query = {
                "query": {
                    "bool": {
                        "must": query_dimensions,
                        "filter": {
                            "bool": {
                                "must": query_filter
                            }
                        }
                    },
                },
                "size": 0,
                "aggs": {
                    "values": {
                        "terms": {
                            "field": dim,
                            "size": 999
                        }
                    }
                }
            }
            dimension_unique_values_count = self.es.query(
                self.es.idx_data+"*",
                query)['aggregations']['values']['buckets']
            keys = [el["key"] for el in dimension_unique_values_count]
            doc_counts = [
                el["doc_count"] for el in dimension_unique_values_count
            ]
            categories = pd.DataFrame(columns=["id", "count"])
            categories["id"] = keys
            categories["count"] = doc_counts
            categories = categories.sort_values(by='count', ascending=False)
            categories = categories.id.tolist()
            # the query above was just to find out all categories in the current datasets
            # this is neccesary to get the same color scheme in this map as in the others (like StackedBarChart)
            table[snd_dim] = table[snd_dim].astype('category').apply(
                lambda x: categories.index(x))
        elif snd_dim_type == 'integer' or snd_dim_type == 'double':
            # if there are only 12 uniques then use them as categories
            # else create 10 equidistant intervals
            if len(table[snd_dim].unique()) > 12:
                table[snd_dim] = pd.cut(table[snd_dim], 10).map(str)
            categories = list(pd.CategoricalIndex(table[snd_dim]).categories)
            table[snd_dim] = table[snd_dim].astype('category').apply(
                lambda x: categories.index(x))
        elif snd_dim_type == 'date':
            table[snd_dim] = pd.to_datetime(table[snd_dim])
            # Dates will either be divided in years
            table[snd_dim] = table[snd_dim].apply(lambda x: str(x.year))
            categories = list(
                pd.CategoricalIndex(table[snd_dim]).categories)
            table[snd_dim] = table[snd_dim].astype('category').apply(
                lambda x: categories.index(x))
        # reorganize the data
        data = dict()
        for hit in table.to_dict(orient='index').values():
            for label in dimensions:
                if label not in data:
                    data[label] = list()
                data[label].append(hit[label])
        data['categories'] = categories
        data['dimensions'] = [fst_dim, snd_dim]
        return data

    def plot_geo_heatmap(self,
                         dimensions,
                         selected_dimensions,
                         filter_options,
                         config,
                         bounds=[[-180, -90], [180, 90]]):
        """Fetch data for geographic heatmaps (either numeric or frequency)"""
        def cartesian(lists):
            """Cartesian product"""
            if len(lists) == 0:
                return [()]
            return [
                x + (y, ) for x in cartesian(lists[:-1]) for y in lists[-1]
            ]
        # check if there are more dimensions to plot, then prepare numeric heatmap
        fst_dim = dimensions[0]
        snd_dim = ''
        if len(dimensions) > 1:
            fst_dim = dimensions[1]
            snd_dim_type = ''
            for dim in dimensions:
                snd_dim = dim
                snd_dim_type = self.api.dimension_properties(
                    [snd_dim])[0]['_source']['type']
                if snd_dim_type != 'geoshape':
                    break
                fst_dim = snd_dim
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        query_filter = self.query.filter(False, '_', filter_options)
        # define grid size over given envelope
        num_x = 30
        num_y = 20
        grid_x = np.linspace(bounds[0][0], bounds[1][0], num_x)
        grid_y = np.linspace(bounds[0][1], bounds[1][1], num_y)
        # define grid points for building envelope
        upperleft = cartesian([grid_x[:-1], grid_y[:-1]])
        lowerright = cartesian([grid_x[1:], grid_y[1:]])
        middle_points = list()
        query_arr = list()
        # create query for every grid tile
        for i in range(len(upperleft)):
            # compute the middle point of grid tile
            middle = np.divide(np.add(upperleft[i], lowerright[i]), 2)
            middle = middle[-1::-1]
            middle_points.append(middle)
            query_geo = {
                "geo_shape": {
                    fst_dim: {
                        "shape": {
                            "type": 'envelope',
                            "coordinates": (upperleft[i], lowerright[i])
                        },
                        "relation": 'INTERSECTS'
                    }
                }
            }
            filter = [query_geo] + query_filter
            query = {
                "query": {
                    "bool": {
                        "must": query_dimensions,
                        "filter": {
                            "bool": {
                                "must": filter
                            }
                        }
                    },
                },
                'size': 0
            }
            # Add aggregation for numeric heatmap
            if len(dimensions) > 1:
                query["aggs"] = {
                    "avg": {
                        "avg": {
                            "field": snd_dim
                        }
                    },
                }
            query_arr.append({'index': self.es.idx_data+"*"})
            query_arr.append(query)
        request = ''
        for each in query_arr:
            request += '%s \n' % json.dumps(each)
        resp = self.es.mquery(query_arr)
        ret_val = dict()
        # if numeric heatmap
        if len(dimensions) > 1:
            geo_avg = [
                r['aggregations']['avg']['value'] for r in resp['responses']
            ]
            geo_avg = np.array(geo_avg).reshape(len(geo_avg), 1)
            # get indicies were geo_avg isn't None (this makes problems)
            indicies = (geo_avg != np.array(None)).ravel()
            middle_points = np.array(middle_points)
            geo_max = np.max(geo_avg[indicies])
            geo_min = np.min(geo_avg[indicies])
            # minmax scaling
            if geo_max != geo_min:
                geo_avg[geo_avg != np.array(None)] = (
                    geo_avg[geo_avg != np.array(None)] - geo_min) / (
                    geo_max - geo_min)
            # add small offset to make a diffrence between real zero values and None values
            offset = 0.01
            geo_avg[indicies] += 0.01
            geo_avg[~indicies] = 0
            data = np.concatenate((middle_points, geo_avg), axis=-1)
            ret_val = {
                'data': data,
                'max': geo_max,
                'min': geo_min,
                'offset': offset,
                'name': snd_dim
            }
        else:
            # case for the normal frequency heatmap
            geo_counts = [r['hits']['total'] for r in resp['responses']]
            geo_counts = np.array(geo_counts).reshape(len(geo_counts), 1)
            geo_max = np.max(geo_counts)
            geo_min = np.min(geo_counts)
            middle_points = np.array(middle_points)
            # minmax scaling
            if geo_max != 0:
                geo_counts = (geo_counts - geo_min) / (geo_max - geo_min)
            data = np.concatenate((middle_points, geo_counts), axis=-1)
            ret_val = {
                'data': data,
                'max': geo_max,
                'min': geo_min,
                'offset': 0,
                'name': 'Frequency'
            }
        return ret_val
