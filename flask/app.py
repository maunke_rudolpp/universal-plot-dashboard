# =========
# LIBRARIES
# =========

from flask import Flask, render_template, request, jsonify, redirect, make_response
import json
import pandas as pd
from os import path
import datetime

dir_path = path.dirname(path.realpath(__file__))
project_path = path.dirname(dir_path)

# ==============
# Import Modules
# ==============

# import project related libraries
import sys
sys.path.insert(0, project_path + '/elasticsearch')
from wrapper_es import WrapperES
import modules.api

# ========================
# CONFIGURATION PARAMETERS
# ========================
config = dict()
with open(project_path + '/config.json', 'r') as f:
    config = json.load(f)

# =====
# FLASK
# =====

app = Flask(__name__)
wrapper = WrapperES(config['ELASTICSEARCH']['host'],
                    config['ELASTICSEARCH']['port'])
api = modules.api.API(wrapper)


@app.route('/')
def index():
    """Main Page"""
    return render_template('index.html')


@app.route('/_select_dimensions', methods=['POST'])
def _tbody_choose_dimension():
    """AJAX Call: Get dimension table for current selected dimensions"""
    selected_dimensions = request.get_json()['selected_dimensions']
    return api.plot_dimensions_table(selected_dimensions)


@app.route('/_filter_data', methods=['POST'])
def _filter_data():
    """AJAX Call: Get feedback if a filter is set"""
    request_obj = request.get_json()
    dimension = request_obj['dimension']
    selected_dimensions = request_obj['selected_dimensions']
    filter_options = request_obj['filter_options']
    request_options = request_obj['request_options']
    return api.filter_data(dimension, selected_dimensions, filter_options,
                           request_options)


@app.route('/_plot_dashboard', methods=['POST'])
def _plot_dashboard():
    """AJAX Call: Get all dat for plotting choosen dashboard"""
    filter = request.get_json()['filter']
    layout = request.get_json()['layout']
    selected_dimensions = request.get_json()['selected_dimensions']
    return api.dashboard(selected_dimensions, filter, layout)


@app.route('/_get_tsv', methods=['POST'])
def _get_tsv():
    """AJAX Call: Offer tsv file from all data on whcih filter apply"""
    filter_options = request.get_json()['filter_options']
    selected_dimensions = request.get_json()['selected_dimensions']
    # we want all the data thus num scroll just needs to be very large
    config = {'num_scrolls': 30000000}
    result = api.get_scroll_data(
        selected_dimensions, selected_dimensions, filter_options, config)
    data = dict()
    # generate tsv from selected dimensions
    data['unique_id'] = list()
    for hit in result:
        data['unique_id'].append(hit['_id'])
        for label in selected_dimensions:
            if label not in data:
                data[label] = list()
            data[label].append(hit['_source'][label])
    data = pd.DataFrame(data).to_csv(sep='\t', encoding='utf-8', index=False)
    response = make_response(data)
    cd = 'attachment; filename=data_' + datetime.datetime.now().isoformat() + '.tsv'
    response.headers['Content-Disposition'] = cd
    response.mimetype = 'text/tsv'
    return response


@app.route('/_server_status', methods=['GET'])
def _server_status():
    """AJAX Call: Check server status and give feedback"""
    num_es_indicies = 0
    # check if indicies exist in elasticsearch or elasticsearch runs if not jump to configuration
    try:
        num_es_indicies = len(wrapper.es.indices.get_alias("*"))
    except:
        pass
    if num_es_indicies == 0:
        return json.dumps({"message": "offline"})
    return json.dumps({"message": "online"})


@app.route('/_total_number_datasets', methods=['GET'])
def _total_number_datasets():
    """AJAX Call: Give total number datasets back"""
    number = api.total_number_datasets()
    return json.dumps({"total_number_datasets": number})


@app.route('/_dashboard_number_datasets', methods=['POST'])
def _dashboard_number_datasets():
    """AJAX Call: Give current number datasets back"""
    request_obj = request.get_json()
    selected_dimensions = request_obj['selected_dimensions']
    filter_options = request_obj['filter_options']
    number = api.dashboard_number_datasets(selected_dimensions, filter_options)
    return json.dumps({"dashboard_number_datasets": number})


@app.route('/_get_heatmap', methods=['POST'])
def _plot_heatmap():
    """AJAX Call: The geographic heatmap needs new data if certain events are triggered (e.g. zoom in)"""
    filter_options = request.get_json()['filter']
    layout = request.get_json()['layout']
    selected_dimensions = request.get_json()['selected_dimensions']
    latlong = request.get_json()['bounds']
    # set bounds (polygon of current displayed map)
    # it shouldn't be outside the longitude and lattitude intervals
    bounds = [[-180, -90], [180, 90]]
    bounds[0][0] = max(latlong['_southWest']['lng'], -180)
    bounds[0][1] = max(latlong['_southWest']['lat'], -90)
    bounds[1][0] = min(latlong['_northEast']['lng'], 180)
    bounds[1][1] = min(latlong['_northEast']['lat'], 90)
    return json.dumps(
        api.plot.plot_geo_heatmap(layout['dimensions'],
                                  selected_dimensions, filter_options,
                                  layout['config'], bounds),
        cls=modules.api.NumpyEncoder)


if __name__ == '__main__':
    app.run(threaded=True, debug=True,
            port=int(config['FLASK']['port']), host=config['FLASK']['host'])
