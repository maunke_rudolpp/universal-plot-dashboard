import pandas as pd
import numpy as np
import sys
# relative path for getting the library
sys.path.insert(0, '../elasticsearch')
from wrapper_es import WrapperES
from shapely.wkt import loads
import shapely.geometry
import datetime

def tryconvert(obj):
    try:
        obj = loads(obj).wkt
        return obj
    except:
        return np.nan

# load data ( get it from here https://data.cityofnewyork.us/Social-Services/sandyrelated/fs5z-tpv4)
data = pd.read_csv('sandyrelated.csv')
# change location to wkt
data['Location'] = 'POINT (' + data['Longitude'].map(str) + ' ' + data['Latitude'].map(str)+')'
data['Location'] = data['Location'].apply(lambda x: tryconvert(x))
# change date to  ISO 8601 formate
dates = ['Created Date','Closed Date', 'Due Date', 'Resolution Action Updated Date']
for nod in dates:
    data[nod] = pd.to_datetime(data[nod], errors='ignore')
    data[nod] = data[nod].apply(lambda x: datetime.datetime.strftime(x, '%Y-%m-%dT%H:%M:%S') if str(x) != 'NaT'  else np.nan)
# change localhost and port to your elasticsearch configuration
wrapper = WrapperES("localhost", 9200)
# load in column information and update it into es
df = pd.read_csv('columns.tsv', sep='\t')
wrapper.create_description_index(df)
# upload data to es with indesx sandyrelated
id_data = 'sandyrelated'
# set unique key for an easier access to the data(not always needed)
df  = data.set_index('Unique Key')
# create mapping for the index
wrapper.create_data_index(id_data,df)
# upload the data...
wrapper.upload_data(id_data,df)
