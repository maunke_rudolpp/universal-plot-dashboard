// =============================================================================
// PLOTS
// =============================================================================
$.fn.extend({
  plot: {
    listener: {
      // plot.listener
      Histogram_Date: function(id) {
        var buttons = '\
            <div style="position:absolute;top:15px;left:30px;">\
            <form id="histogram_date_interval_' + id + '" style="margin:0;padding:0;" action="#">\
              <label>\
                <input name="interval" type="radio" />\
                <span>year</span>\
              </label>\
              <label>\
                <input name="interval" type="radio" />\
                <span>month</span>\
              </label>\
              <label>\
                <input name="interval" type="radio" />\
                <span>day</span>\
              </label>\
            </form>\
          </div>';
        $("#plot_dashboard_body_tile_" + id).append(buttons);
        var interval_active = global.dashboard.layout_options[id].config.interval;
        var buttons_el = $("#histogram_date_interval_" + id + " label");
        buttons_el.each(function() {
          if ($(this).find("span").html() == interval_active) {
            $(this).find("input").attr('checked', true);
          } else {
            $(this).find("input").attr('checked', false);
          }
        });

        $("#histogram_date_interval_" + id).off().change(function() {
          var interval = $("#histogram_date_interval_" + id).find(":checked").next('span').html();
          console.log("Update Tile" + id + " to interval: " + interval);
          var id_layout = global.dashboard.layout_options[id];
          id_layout.config.interval = interval;
          var plot_dashboard_request = {
            layout: [id_layout],
            selected_dimensions: global.dashboard.selected_dimensions,
            filter: global.dashboard.filter_options
          }
          global.dashboard.layout_options[id] = id_layout;
          $.ajax({
            type: 'POST',
            url: "/_plot_dashboard",
            data: JSON.stringify(plot_dashboard_request),
            dataType: 'json',
            contentType: 'application/json',
            timeout: global.config.ajax_timeout
          }).done(function(data) {
            $("#plot_dashboard_body_tile_" + id).find("*").off().remove();
            $(this).plot.dashboard.Histogram(id, id_layout, data[0]);
            $(this).event.progress(false);
          }).fail(function() {
            M.toast({
              html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
              displayLength: 4000
            });
            $(this).event.progress(false);
            console.log("Failed.");
          });
        });
      }
    },
    helper: {
      // plot.helper
      build_title: function(id, layout_option) {
        var title = layout_option.title;
        var title_div = '<div style="position:absolute;z-index:999;width:100%;top:0;left:0;color:#90A4AE;font-size:18;text-align:center;" class="flow-text"><span style="background:#fafafa;border:8px #fafafa solid;border-radius: 0px 0px 3px 3px;">' + title + '</span></div>';
        $('#plot_dashboard_body_tile_' + id).append(title_div);
      },
      // plot.helper
      getMap: function(div, zoomLevel, center) {

        var southWest = L.latLng(-90, -180),
          northEast = L.latLng(90, 180);
        var bounds = L.latLngBounds(southWest, northEast);
        var map = L.map(div, {
          minZoom: 2,
          maxZoom: zoomLevel
        }).setView(center, 2);
        var baseLayers = {
          "Grayscale": L.esri.basemapLayer("DarkGray", {
            noWrap: true,
            reuseTiles: true,
            bounds: bounds
          }),
          "Topographic": L.esri.basemapLayer("Topographic", {
            noWrap: true,
            reuseTiles: true,
            bounds: bounds
          }),
          "Streets": L.esri.basemapLayer("Streets", {
            noWrap: true,
            reuseTiles: true,
            bounds: bounds
          }),
          "Oceans": L.esri.basemapLayer("Oceans", {
            noWrap: true,
            reuseTiles: true,
            bounds: bounds
          }),
          "Imagery": L.esri.basemapLayer("Imagery", {
            noWrap: true,
            reuseTiles: true,
            bounds: bounds
          })
        };
        baseLayers["Topographic"].addTo(map);
        var control = L.control.layers(baseLayers);
        control.addTo(map);
        return map;
      },
      // plot.helper
      getAreaColor: function(feature) {
        var c_pool = colorscheme.plot.dashboard.colorpool;
        return c_pool[feature % c_pool.length];
      },
      // plot.helper
      getAreaStyle: function(feature) {
        return {
          fillColor: $(this).plot.helper.getAreaColor(feature),
          color: $(this).plot.helper.getAreaColor(feature),
          weight: 1,
          opacity: 0.3,
          fillOpacity: 0.3
        }
      },
      // plot.helper
      getCol: function(matrix, col) {
        var column = [];
        for (var i = 0; i < matrix.length; i++) {
          column.push(matrix[i][col]);
        }
        return column;
      },
      // plot.helper
      load_idw: function(map, data) {
        var idwLayer, gradient = {
          0: 'transparent'
        };
        // delete old idw layer (for renewing)
        map.eachLayer(function(e) {
          if (e._idw) {
            map.removeLayer(e);
          }
        });
        // get color scheme
        for (let i = 1; i < colorscheme.plot.dashboard.colorscale.length; i++) {
          gradient[colorscheme.plot.dashboard.colorscale[i][0]] = colorscheme.plot.dashboard.colorscale[i][1];
        }
        idwLayer = L.idwLayer(data.data, {
          opacity: 0.4,
          maxZoom: 10,
          cellSize: 15,
          exp: 5,
          gradient: gradient
        });
        idwLayer.addTo(map);
      },
      // plot.helper
      load_legend: function(map, data, layout_option, id) {
        var gradient = [],
          range = [],
          legend, color;
        // get color for building legend and compute real values (idw takes only values from 0 to 1)
        for (let i = 0; i < colorscheme.plot.dashboard.colorscale.length; i++) {
          range.push(colorscheme.plot.dashboard.colorscale[i][1]);
          let t = ((colorscheme.plot.dashboard.colorscale[i][0] - data.offset) * (data.max - data.min) + data.min).toFixed(0);
          gradient.push(t);
        }
        // set legend min und max according to data
        gradient[0] = data.min;
        gradient[gradient.length - 1] = data.max;
        color = d3.scale.threshold()
          .domain(gradient)
          .range(range);

        legend = L.control({
          position: 'bottomright'
        });
        // remove d3 legend (for renewing)
        d3.select(".legend_" + id).remove();

        legend.onAdd = function(map) {
          var div = L.DomUtil.create('div', 'legend_' + id + ' legend_heatmap');
          return div
        };

        legend.addTo(map);
        // just do d3 thing for building legend
        var x = d3.scale.linear()
          .domain([data.min, data.max])
          .range([0, 400]);
        var xAxis = d3.svg.axis()
          .scale(x)
          .orient("top")
          .tickFormat(d3.format("d"))
          .tickSize(1)
          .tickValues(color.domain());

        var svg = d3.select('.legend_' + id).append("svg")
          .attr("id", 'legend_' + id)
          .attr("width", 450)
          .attr("height", 40);
        var g = svg.append("g")
          .attr("class", "key")
          .attr("transform", "translate(25,16)");
        g.selectAll("rect")
          .data(color.range().map(function(d, i) {
            return {
              x0: i ? x(color.domain()[i - 1]) : x.range()[0],
              x1: i < color.domain().length ? x(color.domain()[i]) : x.range()[1],
              z: d
            };
          }))
          .enter().append("rect")
          .attr("height", 10)
          .attr("x", function(d) {
            return d.x0;
          })
          .attr("width", function(d) {
            return d.x1 - d.x0;
          })
          .style("fill", function(d) {
            return d.z;
          });
        g.call(xAxis).append("text")
          .attr("class", "caption")
          .attr("y", 21)
          .text(data.name);
      }
    },
    filter_data: {
      // plot.filter_data
      date: function(id_name, data, interval) {
        var plot_data = [{
          x: $(this).plot.helper.getCol(data, 0),
          y: $(this).plot.helper.getCol(data, 1),
          type: 'bar',
          marker: {
            color: 'rgb(174,213,129)'
          }
        }];

        var plot_layout = {
          xaxis: {
            title: 'Date',
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            type: 'date',
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            fixedrange: true,
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 50,
            t: 20,
            pad: 5
          }
        };
        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('filter_data_date', plot_data, plot_layout, plot_config);
      },
      // plot.filter_data
      numeric: function(id_name, data) {
        var plot_data = [{
          x: $(this).plot.helper.getCol(data, 0),
          y: $(this).plot.helper.getCol(data, 1),
          type: 'bar',
          marker: {
            color: 'rgb(174,213,129)'
          }
        }];

        var plot_layout = {
          xaxis: {
            title: 'Value',
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            fixedrange: true,
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            fixedrange: true,
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 50,
            t: 20,
            pad: 5
          }
        };
        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('filter_data_numeric', plot_data, plot_layout, plot_config);
      },
      // plot.filter_data
      geo: function(id_name, features) {
        var drawnItems = new L.geoJson(),
          map = $(this).plot.helper.getMap('filter_data_geo', 15, [0, 0]);
        map.options['drawControl'] = true;
        if (features) {
          drawnItems = L.geoJson(features).setStyle({
            color: '#AED581',
            fillColor: '#AED581'
          });
        }
        map.addLayer(drawnItems);
        map.addControl(new L.Control.Draw({
          position: 'bottomright',
          edit: false,
          draw: {
            marker: false,
            circlemarker: false,
            circle: false,
            polyline: false,
            polygon: {
              allowIntersection: false,
              showArea: true,
              shapeOptions: {
                color: '#90A4AE'
              }
            },
            rectangle: {
              shapeOptions: {
                color: '#90A4AE'
              }
            }
          }
        }));
        map.on(L.Draw.Event.CREATED, function(event) {
          let layer = event.layer;
          layer.setStyle({
            color: '#AED581',
            fillColor: '#AED581'
          });
          drawnItems.addLayer(layer);
        });
        return drawnItems;
      }
    },
    dashboard: {
      // plot.dashboard
      GeoHeatmap: function(id, layout_option, data) {
        var map = $(this).plot.helper.getMap('plot_dashboard_body_tile_' + id, 15, [0, 0]);
        $(this).plot.helper.load_idw(map, data);
        $(this).plot.helper.load_legend(map, data, layout_option, id);
        var ajaxObj = true;
        map.on('dragend zoomend', function() {
          let mapObj = $(this)[0];
          ajaxObj = $.ajax({
            type: 'POST',
            url: "/_get_heatmap",
            data: JSON.stringify({
              "bounds": map.getBounds(),
              "selected_dimensions": global.dashboard.selected_dimensions,
              "filter": global.dashboard.filter_options,
              "layout": layout_option
            }),
            dataType: 'json',
            contentType: 'application/json',
            beforeSend: function() {
              if (ajaxObj instanceof Object && ajaxObj.readyState < 4) {
                ajaxObj.abort();
              }
            },
            timeout: global.config.ajax_timeout
          }).done(function(e) {
            const data = e;
            $(this).plot.helper.load_idw(mapObj, data);
            $(this).plot.helper.load_legend(map, data, layout_option, id);
          });
        });
        $(this).plot.helper.build_title(id, layout_option);
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      },
      // plot.dashboard
      GeoNumHeatmap: function(id, layout_option, data) {
        $(this).plot.dashboard.GeoHeatmap(id, layout_option, data);
      },
      // plot.dashboard
      PolygonMap: function(id, layout_option, data) {
        var control;
        map = $(this).plot.helper.getMap('plot_dashboard_body_tile_' + id, 15, [0, 0]);
        L.geoJson(data).addTo(map);
        $(this).plot.helper.build_title(id, layout_option);
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      },
      // plot.dashboard
      CategoricalMap: function(id, layout_option, data) {
        var overLayers = [];
        var control, map;
        var categories = data['categories'];
        var dim = data['dimensions'];

        control = L.control.layers(null, null, {
          position: 'bottomright',
          collapsed: false
        });
        for (i = 0; i < categories.length; i++) {
          overLayers.push(L.layerGroup());
        }
        map = $(this).plot.helper.getMap('plot_dashboard_body_tile_' + id, 15, [0, 0]);
        for (i = 0; i < data[dim[0]].length; i++) {
          console.log(data[dim[0]][i]['type'])
          if (data[dim[0]][i]['type'] == 'Point') {
            L.geoJson(data[dim[0]][i], {
              pointToLayer: function(feature, latlng) {
                return L.circleMarker(latlng, {
                  radius: 8,
                  fillColor: $(this).plot.helper.getAreaColor(data[dim[1]][i]),
                  color: $(this).plot.helper.getAreaColor(data[dim[1]][i]),
                  weight: 1,
                  opacity: 1,
                  fillOpacity: 0.8
                });
              }
            }).addTo(overLayers[data[dim[1]][i]]);
          } else {
            L.geoJson(data[dim[0]][i], {
              style: $(this).plot.helper.getAreaStyle(data[dim[1]][i]),
            }).addTo(overLayers[data[dim[1]][i]]);
          }
        }
        for (i = 0; i < categories.length; i++) {
          // don't categories with zero polygons
          if (Object.keys(overLayers[i]._layers).length > 0) {
            overLayers[i].addTo(map);
            control.addOverlay(overLayers[i],
              '<div class="legend" style="background:' +
              $(this).plot.helper.getAreaColor(i) + '"></div><span styel=" display: inline-block;  vertical-align: middle;line-height: normal;"">' +
              categories[i] + '</span>');
          }
        }
        control.addTo(map);
        control._container.classList.add("info")
        $(this).plot.helper.build_title(id, layout_option);
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      },
      // plot.dashboard
      Scatter: function(id, layout_option, data) {
        var plot_data = [{
          x: data[layout_option.dimensions[0]],
          y: data[layout_option.dimensions[1]],
          mode: 'markers',
          type: 'scatter',
          marker: {
            color: 'rgb(100,181,246)'
          }
        }];

        var plot_layout = {
          title: layout_option.title,
          font: {
            size: 18,
            color: 'rgb(144,164,174)'
          },
          xaxis: {
            title: layout_option.dimensions[0],
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          yaxis: {
            title: layout_option.dimensions[1],
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 50,
            t: 52,
            pad: 5
          },
          showlegend: true,
        };
        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('plot_dashboard_body_tile_' + id, plot_data, plot_layout, plot_config);
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      },
      // plot.dashboard
      Histogram: function(id, layout_option, plot_data) {
        var data = JSON.parse(plot_data)['data'];
        var plot_data = [{
          x: $(this).plot.helper.getCol(data, 0),
          y: $(this).plot.helper.getCol(data, 1),
          type: 'bar',
          marker: {
            color: 'rgb(174,213,129)'
          }
        }];

        var plot_layout = {
          title: layout_option.title,
          font: {
            size: 18,
            color: 'rgb(144,164,174)'
          },
          xaxis: {
            title: layout_option.dimensions[0],
            font: {
              size: 18,
              color: 'rgb(144,164,174)'
            },
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            fixedrange: true,
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 50,
            t: 52,
            pad: 5
          },
        };

        var id_type = $(this).api.helper.id_type(layout_option.dimensions[0]);
        if (id_type == "date") {
          plot_layout.xaxis['type'] = "date";
        } else if ((id_type == "keyword") && data.length > 6) {
          plot_layout.margin.b = 100;
        }
        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('plot_dashboard_body_tile_' + id, plot_data, plot_layout, plot_config);
        if (id_type == "date") {
          $(this).plot.listener.Histogram_Date(id);
        }
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      },
      Heatmap: function(id, layout_option, data) {
        var z_min = Math.min(...data["counts"]);
        var z_max = Math.max(...data["counts"]);

        var plot_data = [{
          x: data["heat_" + layout_option.dimensions[0]],
          y: data["heat_" + layout_option.dimensions[1]],
          z: data["heat_counts"],
          type: 'contour',
          autocontour: false,
          contours: {
            start: z_min,
            end: z_max,
            size: (z_max - z_min) / 40.0,
            showlines: false
          },
          colorscale: colorscheme.plot.dashboard.colorscale,
          colorbar: {
            title: 'Counts',
            titleside: 'top',
            titlefont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
          }
        }];

        var plot_layout = {
          title: layout_option.title,
          font: {
            size: 18,
            color: 'rgb(144,164,174)'
          },
          xaxis: {
            title: layout_option.dimensions[0],
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            autorange: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          yaxis: {
            title: layout_option.dimensions[1],
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            autorange: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 60,
            t: 52,
            pad: 5
          },
        };

        var id_type = $(this).api.helper.id_type(layout_option.dimensions[0]);
        if (id_type == "date") {
          plot_layout.xaxis['type'] = "date";
        } else if (id_type == "keyword") {
          plot_layout.margin.b = 120;
          plot_data[0].type = "heatmap";
          plot_data[0].colorscale = colorscheme.plot.dashboard.colorscale2;
          delete plot_data[0]["autocontour"];
          delete plot_data[0]["contours"];
        }
        var id_type = $(this).api.helper.id_type(layout_option.dimensions[1]);
        if (id_type == "date") {
          plot_layout.yaxis['type'] = "date";
        } else if (id_type == "keyword") {
          plot_layout.margin.l = 120;
          plot_data[0].type = "heatmap";
          plot_data[0].colorscale = colorscheme.plot.dashboard.colorscale2;
          delete plot_data[0]["autocontour"];
          delete plot_data[0]["contours"];
        }

        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('plot_dashboard_body_tile_' + id, plot_data, plot_layout, plot_config);
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      },
      // plot.dashboard
      StackedBarChart: function(id, layout_option, data) {
        var c_pool = colorscheme.plot.dashboard.colorpool;
        var plot_data = [];
        for (var row in data["heat_" + layout_option.dimensions[1]]) {
          var plot_bar = {
            x: data["heat_" + layout_option.dimensions[0]],
            y: data["heat_counts"][row],
            name: data["heat_" + layout_option.dimensions[1]][row],
            type: 'bar',
            marker: {
              color: c_pool[row % c_pool.length]
            }
          }
          plot_data.push(plot_bar);
        }

        var plot_layout = {
          title: layout_option.title,
          font: {
            size: 18,
            color: 'rgb(144,164,174)'
          },
          xaxis: {
            title: layout_option.dimensions[0],
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          yaxis: {
            title: "Counts of " + layout_option.dimensions[1],
            titlefont: {
              size: 16,
              color: 'rgb(144,164,174)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(144,164,174)'
            },
            fixedrange: true,
            autorange: true,
            showgrid: true,
            zeroline: false,
            showline: false,
            autotick: true,
            ticks: '',
            showticklabels: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 60,
            t: 52,
            pad: 5
          },
          barmode: 'stack',
          showlegend: true,
        };

        var id_type = $(this).api.helper.id_type(layout_option.dimensions[0]);
        if (id_type == "date") {
          plot_layout.xaxis['type'] = "date";
        } else if (id_type == "keyword") {
          plot_layout.margin.b = 120;
        }

        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('plot_dashboard_body_tile_' + id, plot_data, plot_layout, plot_config);
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      },
      // plot.dashboard
      PieChart: function(id, layout_option, plot_data) {
        var c_pool = colorscheme.plot.dashboard.colorpool;
        var data = JSON.parse(plot_data)['data'];
        var plot_data = [{
          labels: $(this).plot.helper.getCol(data, 0),
          values: $(this).plot.helper.getCol(data, 1),
          type: 'pie',
          marker: {
            colors: c_pool
          },
          insidetextfont: {
            color: '#fafafa'
          },
          outsidetextfont: {
            color: '#90A4AE'
          }
        }];

        var plot_layout = {
          title: layout_option.title,
          font: {
            size: 18,
            color: 'rgb(144,164,174)'
          },
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 50,
            t: 52,
            pad: 5
          },
          showlegend: true,
        };

        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('plot_dashboard_body_tile_' + id, plot_data, plot_layout, plot_config);
        $(this).event.progress(false);
        $('#plot_dashboard_body_tile_' + id).fadeIn(500);
      }
    }
  },
});
