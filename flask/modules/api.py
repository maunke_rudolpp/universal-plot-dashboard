# ========================================
# Visualisierung, FLASK API
# Jonas Gütter, Paul Rudolph, Markus Unkel
# ========================================

# =========
# LIBRARIES
# =========

import sys
import json
import pandas as pd
import numpy as np
from shapely.wkt import loads
from shapely.geometry import mapping, shape, MultiPolygon
sys.path.append('../../elasticsearch')
import modules.plot as plot

# =========
# API QUERY
# =========


class API_QUERY():
    """API Elasticsearch queries for building filters"""

    def __init__(self):
        self.filter_map = {
            "keyword": self.filter_categorial,
            "date": self.filter_date,
            "integer": self.filter_numeric,
            "double": self.filter_numeric,
            "geoshape": self.filter_geo
        }

    def dimensions(self, dimensions):
        """Query for existing dimensions"""
        query = [{"exists": {"field": str(dim)}} for dim in dimensions]
        return query

    def filter_categorial(self, option):
        """Query for terms which should be in categorical types"""
        return {"terms": {option['id']: option['config']['keywords']}}

    def filter_date(self, option):
        """Query for date intervals"""
        return {
            "range": {
                option['id']: {
                    "gte": option['config']['gte'],
                    "lt": option['config']['lte'],
                    "format": "epoch_millis"
                }
            }
        }

    def filter_numeric(self, option):
        """Query for numeric interval"""
        return {
            "range": {
                option['id']: {
                    "gte": option['config']['min'],
                    "lt": option['config']['max']
                }
            }
        }

    def filter_geo(self, option):
        """Query for geographic filter"""
        # transform selected polygons to multipolygon
        polygons = [shape(i['geometry']) for i in option['config']['features']]
        multi_polygon = mapping(MultiPolygon(polygons))
        relation = option['config']['relation']
        # Catch if the filter is empty
        if len(polygons) == 0:
            relation = 'DISJOINT'
        query = {
            "geo_shape": {
                option['id']: {
                    "shape": {
                        "type": multi_polygon['type'],
                        "coordinates": multi_polygon['coordinates']
                    },
                    "relation": relation
                }
            }
        }
        return query

    def filter(self, is_filter_data, dimension, options):
        """Get all filter options of custom dashboard"""
        query = list()
        for opt in options:
            id_name = opt['id']
            id_type = opt['type']
            if id_name != dimension or not is_filter_data:
                query.append(self.filter_map[id_type](opt))
        return query


# ===
# API
# ===


class API():
    """API for elasticsearch and flask app"""

    def __init__(self, es):
        self.es = es
        self.config = {'max_result_window': 9999}
        self.query = API_QUERY()
        self.plot = plot.API_PLOT(self.es, self, self.query, self.config)

    def get_scroll_data(self, dimensions, selected_dimensions, filter_options,
                        config):
        """Scrolling queries  in es will be handled here"""
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        # generate filter options '_' is dummy for a dimension which normally will be excluded
        # we don't have anything to exclude here
        query_filter = self.query.filter(False, '_', filter_options)
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": self.config['max_result_window'],
            "_source": dimensions
        }
        num_scrolls = config['num_scrolls']
        result = self.es.squery(self.es.idx_data + "*", query, num_scrolls)
        return result

    def total_number_datasets(self):
        """Fetches the total number of datasets"""
        query = {"query": {"match_all": {}}, "size": 0}
        return self.es.query(self.es.idx_data + "*", query)['hits']['total']

    def dashboard_number_datasets(self, selected_dimensions, filter_options):
        """Fetch the total number of datasets applied with the filter options"""
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)

        query_filter = self.query.filter(False, None, filter_options)
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0
        }
        return self.es.query(self.es.idx_data + "*", query)['hits']['total']

    def dimension_properties(self, dimension_list):
        """Fetch the properties (e.g. type) of the given dimensions"""
        query = {
            "query": {
                "terms": {
                    "_id": dimension_list
                }
            },
            "size": len(dimension_list),
            "_source":
            ["id", "label", "desc", "hierarchy", "type"]
        }
        return self.es.query(self.es.idx_desc, query)['hits']['hits']

    def dimension_count_query(self, dimension, selected_dimensions):
        """Fetch count of values which exists for one column"""
        query_cols = []
        if selected_dimensions:
            query_cols = [{
                "exists": {
                    "field": str(field_name)
                }
            } for field_name in selected_dimensions]
        if dimension:
            query_cols.append({"exists": {"field": str(dimension)}})
        query = {
            "query": {
                "bool": {
                    "must": query_cols
                },
            },
            "size": 0
        }
        return query

    def dimension_list_count(self, dimension_list, selected_dimensions):
        """Fetch count of values which exists for each column"""
        query_arr = []
        for key_val in dimension_list:
            query_arr.append({'index': self.es.idx_data + "*"})
            query_arr.append(
                self.dimension_count_query(key_val, selected_dimensions))
        request = ''
        for each in query_arr:
            request += '%s \n' % json.dumps(each)
        resp = self.es.mquery(request)

        column_counts = [r['hits']['total'] for r in resp['responses']]

        return column_counts

    def dimension_list_distinct_query(self, dimension, selected_dimensions):
        """Fetch count of distinct values which exists for one column"""
        query_cols = []
        if selected_dimensions:
            query_cols = [{
                "exists": {
                    "field": str(field_name)
                }
            } for field_name in selected_dimensions]
        if dimension:
            query_cols.append({"exists": {"field": str(dimension)}})
        query = {
            "query": {
                "bool": {
                    "must": query_cols
                },
            },
            "size": 0,
            "aggs": {
                "distinct": {
                    "terms": {
                        "field": dimension,
                        "size": 999,  # self.config['max_result_window']
                    }
                }
            }
        }
        return query

    def dimension_list_distinct_counts(self, dimension_list,
                                       selected_dimensions):
        """Fetch count of distinct values which exists for each column"""
        query_arr = []
        for key_val in dimension_list:
            query_arr.append({'index': self.es.idx_data + "*"})
            query_arr.append(
                self.dimension_list_distinct_query(key_val,
                                                   selected_dimensions))

        request = ''
        for each in query_arr:
            request += '%s \n' % json.dumps(each)

        resp = self.es.mquery(request)
        distinct_counts = []
        for r in resp['responses']:
            try:
                counts = len(r['aggregations']['distinct']['buckets'])
                if counts == 999:
                    distinct_counts.append(">=999")
                else:
                    distinct_counts.append(
                        len(r['aggregations']['distinct']['buckets']))
            except:
                distinct_counts.append("")

        return distinct_counts

    def plot_dimensions_table(self, selected_dimensions):
        """Build and execute query for the dimensions table"""
        # get column names
        vals = set()
        for key, values in self.es.es.indices.get_mapping(index="idx_data_*").items():
            dimensions_set = set(
                list(list(values['mappings'].values())[0]['properties'].keys()))
            if dimensions_set >= set(selected_dimensions):
                vals = vals | dimensions_set

        dimensions_ids = list(vals - set(selected_dimensions))
        # get types of unique_dimensions
        dimensions_props = self.dimension_properties(dimensions_ids)
        dimensions_props = [
            prop for prop in dimensions_props
        ]
        # dimension names
        dimensions_names = []
        for i, prop_source in enumerate(dimensions_props):
            prop = prop_source['_source']
            name = ""
            if prop['hierarchy']:
                name += prop['hierarchy'][0] + " | "
            try:
                name += prop['label']
            except:
                print("ERROR_NO_LABEL_IN_DESCRIPTION: COLUMN_ID is",
                      dimensions_ids[i])
            dimensions_names.append(name)
        # overwrite dimensions ids if in description especially dimensions does not appear
        dimensions_ids = [prop['_source']['id'] for prop in dimensions_props]
        # dimensions types
        dimensions_types = [
            prop['_source']['type'] for prop in dimensions_props
        ]
        # dimensions description
        dimensions_desc = []
        for prop in dimensions_props:
            try:
                dimensions_desc.append(prop['_source']['desc'])
            except:
                print("ID", prop['_source']['id'], "HAS NO DESC")
                dimensions_desc.append("No Description")
        # Build dimension table
        table = pd.DataFrame(
            columns=["id", "name", "type", "count", "distinct", "desc"])
        table['id'] = dimensions_ids
        table['name'] = dimensions_names
        table['type'] = dimensions_types
        table['desc'] = dimensions_desc
        # dimension counts
        dimensions_count = self.dimension_list_count(dimensions_ids,
                                                     selected_dimensions)
        table['count'] = dimensions_count
        # dimension distinct value counts, set value as undefined for geo objects
        table['distinct'] = 'undefined'
        # get all ids which aren't geo objects and get distinct values from them using aggregation
        dimension_ids_wgeo = table.loc[table['type']
                                       != 'geoshape', 'id'].tolist()
        dimensions_distinct = self.dimension_list_distinct_counts(
            dimension_ids_wgeo, selected_dimensions)
        table.loc[table['type'] !=
                  'geoshape', 'distinct'] = dimensions_distinct
        return table.to_json(orient='split')

    def _filter_categorial(self, dimension, selected_dimensions,
                           filter_options, request_options):
        """FILTER DATA: Categorial"""
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        is_filter_data = True
        if "query_filtered_data" in request_options:
            is_filter_data = False
        query_filter = self.query.filter(is_filter_data, dimension,
                                         filter_options)

        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "values": {
                    "terms": {
                        "field": dimension,
                        "size": 999
                    }
                }
            }
        }
        # get buckets and create key and count value pair
        dimension_unique_values_count = self.es.query(
            self.es.idx_data + "*", query)['aggregations']['values']['buckets']
        keys = [el["key"] for el in dimension_unique_values_count]
        doc_counts = [el["doc_count"] for el in dimension_unique_values_count]
        table = pd.DataFrame(columns=["id", "count"])
        table["id"] = keys
        table["count"] = doc_counts
        table = table.sort_values(by='count', ascending=False)
        return table.to_json(orient='split')

    def _filter_date(self, dimension, selected_dimensions, filter_options,
                     request_options):
        """FILTER DATA: Date"""
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        is_filter_data = True
        query_filter = self.query.filter(is_filter_data, dimension,
                                         filter_options)
        interval = request_options['interval']
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "values": {
                    "date_histogram": {
                        "field": dimension,
                        "interval": interval
                    }
                }
            }
        }
        # get buckets and create key and count value pair
        date_histogram_buckets = self.es.query(
            self.es.idx_data + "*", query)['aggregations']['values']['buckets']
        keys = [el["key"] for el in date_histogram_buckets]
        doc_counts = [el["doc_count"] for el in date_histogram_buckets]
        table = pd.DataFrame(columns=["key", "count"])
        table["key"] = keys
        table["count"] = doc_counts
        return table.to_json(orient='split')

    def _filter_numeric(self, dimension, selected_dimensions, filter_options,
                        request_options):
        """FILTER DATA: Numeric"""
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # get type of dimension
        dimension_type = self.dimension_properties(
            [dimension])[0]['_source']['type']
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        # is filter dimension, but only want to see min max window ...
        # ... adjusted by user
        is_filter_data = False
        if "undo" in request_options:
            is_filter_data = True
        if "update" in request_options:
            filter_option = {
                "id": dimension,
                "type": dimension_type,
                "config": {
                    "min":
                    request_options["min"],
                    "max":
                    request_options["max"] +
                    (request_options["max"] - request_options["min"]) / 100.0
                }
            }
            filter_options = filter_options + [filter_option]
        query_filter = self.query.filter(is_filter_data, dimension,
                                         filter_options)
        query_range = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "max": {
                    "max": {
                        "field": dimension
                    }
                },
                "min": {
                    "min": {
                        "field": dimension
                    }
                }
            }
        }
        value_range = self.es.query(self.es.idx_data + "*",
                                    query_range)['aggregations']
        value_min = value_range['min']['value']
        value_max = value_range['max']['value']

        # create own interval for buckets
        interval = (value_max - value_min) / 100.0
        if dimension_type == "Integer":
            interval = int(interval)
            if interval < 1:
                interval = 1
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "values": {
                    "histogram": {
                        "field": dimension,
                        "interval": interval
                    }
                }
            }
        }
        numeric_histogram_buckets = self.es.query(
            self.es.idx_data + "*", query)['aggregations']['values']['buckets']
        # get buckets and create key and count value pair
        keys = [el["key"] for el in numeric_histogram_buckets]
        doc_counts = [el["doc_count"] for el in numeric_histogram_buckets]
        table = pd.DataFrame(columns=["key", "count"])
        table["key"] = keys
        table["count"] = doc_counts
        return table.to_json(orient='split')

    def _filter_geo(self, dimension, selected_dimensions, filter_options,
                    request_options):
        """FILTER DATA: Geo"""
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        query_filter = self.query.filter(False, dimension, filter_options)
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0
        }
        total_hits = self.es.query(
            self.es.idx_data + "*", query)['hits']['total']
        return json.dumps({'data': total_hits})

    def filter_data(self, dimension, selected_dimensions, filter_options,
                    request_options):
        """Execute filter according to type of dimension"""
        dimension_type = self.dimension_properties(
            [dimension])[0]['_source']['type']
        print("id:", dimension, " has type", dimension_type)
        filter_map = {
            "keyword": self._filter_categorial,
            "date": self._filter_date,
            "integer": self._filter_numeric,
            "double": self._filter_numeric,
            "geoshape": self._filter_geo
        }
        data = filter_map[dimension_type](dimension, selected_dimensions,
                                          filter_options, request_options)
        return data

    def dashboard(self, selected_dimensions, filter, layout):
        """Fetch data for plots in dashboard"""
        data = list()
        for plot in layout:
            plot_type = plot['option']
            plot_dims = plot['dimensions']
            plot_config = plot['config']
            plot_data = self.plot.plot_map[plot_type](
                plot['dimensions'], selected_dimensions, filter, plot_config)
            data.append(plot_data)
        return json.dumps(data, cls=NumpyEncoder)


class NumpyEncoder(json.JSONEncoder):
    """NumpyEncoder as extend to the JSON encoder"""

    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        if isinstance(obj, np.int64):
            return int(obj)
        return json.JSONEncoder.default(self, obj)
