// =============================================================================
// GLOBAL CONFIGURATION
// =============================================================================
var global = {
  config: {
    ajax_timeout: 100000,
    // url data input: helper variable for plot dashboard ...
    // ... after ajax call get data
    deferred: $.Deferred()
  },
  progress: {
    // all loading progresses added to count
    count: 0
  },
  dashboard: {
    selected_dimensions: [],
    filter_dimensions: [],
    dimensions_table: [],
    dimensions_types: [],
    filter_options: [],
    layout_options: [],
    title: "",
    max_dim_filter_number: 8
  },
  colorscheme: {
    select_dimensions: {
      filter: {
        listed: "light-green lighten-1",
        unlisted: "blue-grey lighten-3"
      }
    }
  },
  standard: {
    month_names: ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ]
  },
  ajax: {
    // ajax variable to prevent select dimensions ajax stacking
    select_dimensions: true
  }
};
// =============================================================================
// GLOBAL COLORSCHEME
// =============================================================================
var colorscheme = {
  plot: {
    dashboard: {
      colorscale: [
        [0, '#ffebee'],
        [0.11, '#ffcdd2'],
        [0.22, '#ef9a9a'],
        [0.33, '#e57373'],
        [0.44, '#ef5350'],
        [0.55, '#f44336'],
        [0.66, '#e53935'],
        [0.77, '#d32f2f'],
        [0.88, '#c62828'],
        [1, '#b71c1c']
      ],
      colorscale2: [
        [0, '#E8EAF6'],
        [0.11, '#C5CAE9'],
        [0.22, '#9FA8DA'],
        [0.33, '#7986CB'],
        [0.44, '#5C6BC0'],
        [0.55, '#3F51B5'],
        [0.66, '#3949AB'],
        [0.77, '#303F9F'],
        [0.88, '#283593'],
        [1, '#1A237E']
      ],
      colorpool: [
        "#f44336", "#2196F3", "#009688", "#CDDC39", "#FF9800", "#795548", "#607D8B",
        "#b71c1c", "#0D47A1", "#004D40", "#827717", "#E65100", "#3E2723", "#263238",
        "#ef9a9a", "#90CAF9", "#80CBC4", "#E6EE9C", "#FFCC80", "#BCAAA4", "#B0BEC5"
      ]
    }
  }
};
// =============================================================================
// DASHBOARD
// =============================================================================
$(document).ready(function() {
  // ===========================================================================
  // MATERIALIZE INITS
  // ===========================================================================
  $('ul.tabs').tabs();
  // tab color
  $(".tabs").css("background-color", "#B0BEC5");
  // tab indicator/underline Color
  $(".tabs>.indicator").css("background-color", '#90A4AE');
  // tab text color
  $(".tabs>li>a").css("color", '#FAFAFA');
  $('.tooltipped').tooltip();
  $('select').formSelect();
  $('.fixed-action-btn').floatingActionButton({
    direction: 'left',
    hoverEnabled: true
  });
  // activate modals
  $('#modal-custom-dashboard').modal({
    onOpenEnd: function() {
      $(this).api.select_dimensions();
    },
    onOpenStart: function() {
      $("#table-choose-dimension").fadeOut(0);
      var elem = $("ul.tabs");
      var instance = M.Tabs.getInstance(elem);
      instance.select('choose_dimension');
    }
  });
  $('#modal-clear-dashboard, #modal-choose-dashboard').modal();
  // activate dropdowns in configuration modal footer
  $('.modal-footer .dropdown-trigger').dropdown({
    coverTrigger: false,
    onOpenStart: function(el) {
      $(".modal-footer").css("height", 60 * global.dashboard.max_dim_filter_number);
      $(el).parent().parent().find("div").css("opacity", 0);
      $(el).parent().css("opacity", 1);
    },
    onCloseEnd: function(el) {
      $(".modal-footer").css("height", "54px");
      $(el).parent().parent().find("div").css("opacity", 1);
    },
  });
  // events on menu button (right bottom)
  // get overview of recent dashboard
  $('#floating_overview').modal({
    onOpenStart: function() {
      // get datasets number
      let val = $("#footer_datasets_dashboard").text();
      $("#floating_overview_size_num").text(val);
      // gets elected dimensions
      $("#floating_overview_selected_dimensions").text(global.dashboard.selected_dimensions.join(", "));
      let content = '';
      // make html content from filter options
      for (let i = 0; i < global.dashboard.filter_options.length; i++) {
        let filter = global.dashboard.filter_options[i];
        let filterContent = '<div class="col s4">' + filter.id + ' (' + filter.type + '):' + '</div>';
        let configString = '';
        for (let key in filter.config) {
          let val = '';
          if (Array.isArray(filter.config[key])) {
            // keywords can be simple added
            if (key == 'keywords') {
              val = filter.config[key].join(", ");

            } else { //geo filter have to be handled here
              val = JSON.stringify(filter.config[key]);
            }
          } else {
            val = filter.config[key];
          }
          configString += '<span>' + key + ": </span><span>" + val + "</span><br>";
        }
        filterContent += '<div class="col s8">' + configString + '</div>';
        content += filterContent;
      }
      $("#floating_overview_filter_options").html(content);
    }
  });
  // share recent dashboard via url
  $("#floating_share_dashboard").click(function() {
    console.log('Generating share link...');
    let output = {
      selected_dimensions: global.dashboard.selected_dimensions,
      filter_dimensions: global.dashboard.filter_dimensions,
      filter_options: global.dashboard.filter_options,
      layout_options: global.dashboard.layout_options,
      title: global.dashboard.title
    };
    let uriLink = window.location.origin;
    if (global.dashboard.layout_options.length > 0) {
      uriLink = window.location.origin + "?data=" + LZString.compressToEncodedURIComponent(JSON.stringify(output));
      console.log(uriLink);
      console.log("Link length:" + uriLink.length);
    }
    // copy to clipboard
    const el = document.createElement('textarea');
    el.value = uriLink;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    M.toast({
      html: "Link for sharing copied to clipboard",
      displayLength: 4000
    });
  });
  // download recent dashboard data, defined through selected dimensions ...
  // ... and filter options
  var floating_download_data_active = false;
  $("#floating_download_data").click(function() {
    if (!floating_download_data_active) {
      if (global.dashboard.selected_dimensions.length > 0) {
        floating_download_data_active = true;
        console.log('Download starting...');
        $(this).event.progress(true);
        // Use XMLHttpRequest instead of jquery $ajax
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          var a;
          if (xhttp.readyState === 4 && xhttp.status === 200) {
            // Trick for making downloadable link
            a = document.createElement('a');
            a.href = window.URL.createObjectURL(xhttp.response);
            a.download = 'data_' + new Date().toISOString() + '.tsv';
            a.style.display = 'none';
            document.body.appendChild(a);
            a.click();
            $(this).event.progress(false);
            floating_download_data_active = false;
            console.log('Download end');
          }
        };
        // Post data to URL which handles post request
        xhttp.open("POST", "/_get_tsv");
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.responseType = 'blob';
        xhttp.send(JSON.stringify({
          'filter_options': global.dashboard.filter_options,
          'selected_dimensions': global.dashboard.selected_dimensions
        }));
        M.toast({
          html: "Server is generating your tsv-file.<br>This process can take several minutes",
          displayLength: 4000
        });
      } else {
        M.toast({
          html: "Please select at least one dimension for downloading the attached data",
          displayLength: 4000
        });
      }
    }
  });
  // =============================================================================
  // CONFIGURATION MODAL
  // =============================================================================
  // click on footer element
  $('#choose_modal_footer div div a').click(function() {
    if ($('span.new', this).length) {
      $('span', this).toggleClass("new");
    }
  });
  // dropdown selected dimensions
  $("#data_dropdown_selected_dimensions").on("click", "li", function() {
    var id_name_arr = $(this).attr('id').split("__");
    var id_name = id_name_arr[id_name_arr.length - 1];
    $(this).remove();
    $(this).api.dashboard.remove_selected_dimension(id_name);
    // update badge of dropdown
    $("#dropdown_selected_dimensions span").html($("#data_dropdown_selected_dimensions li").length);
    // update tbody
    $(this).api.select_dimensions();
    M.toast({
      html: "Plot Dimensions: " + id_name + " removed",
      displayLength: 4000
    });
    $("#filter_data_body").fadeOut(0);
    // delete layout option if deleted dimension is in option
    // delete timestamp layout depended options later
    var timestamps_to_remove = [];
    for (var opt in global.dashboard.layout_options) {
      // run over all dimensions in option
      for (var id in global.dashboard.layout_options[opt].dimensions) {
        var layout_id = global.dashboard.layout_options[opt].dimensions[id];
        if (layout_id == id_name) {
          var timestamp = global.dashboard.layout_options[opt].timestamp;
          timestamps_to_remove.push(timestamp);
          // check for all option elements in dropdown
          $("#data_dropdown_layout_options li").each(function() {
            var timestamp_li = $(this).data("timestamp");
            if (timestamp_li == timestamp) {
              var title = $(this).data("title");
              $(this).remove();
              $("#dropdown_layout_options span").html($("#data_dropdown_layout_options li").length);
              $("#dropdown_layout_options span").removeClass("new");
              M.toast({
                html: "Layout: " + title + " removed",
                displayLength: 6000
              });
            }
          });
        }
      }
    }
    // remove id_name depended layout options global
    for (var i in timestamps_to_remove) {
      $(this).api.dashboard.remove_layout_option(timestamps_to_remove[i]);
    }
    // clear layout tab content with updated selected dimensions
    $(this).api.helper.dashboard_layout_clear();
    // delete filter options for deleted selected dimension
    $("#data_dropdown_filter_data li").each(function() {
      var id_name_arr = $(this).attr('id').split("__");
      var id_name_li = id_name_arr[id_name_arr.length - 1];
      if (id_name_li == id_name) {
        $(this).remove();
        $(this).api.dashboard.remove_filter_option(id_name);
        // update badge of dropdown
        $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length).removeClass("new");
        M.toast({
          html: "Filter Options: " + id_name + " removed",
          displayLength: 4000
        });
        $("#tab_filter_data").trigger("click");
      }
    });
  });
  // on click dropdown selection dimensions & modal header tab_filter_data
  $("#tab_filter_data, #data_dropdown_selected_dimensions").click(function() {
    // remove options
    var select_el = $("#filter_data_select");
    select_el.children().not(":disabled").remove();
    // update options
    // selected dimensions
    for (id in global.dashboard.selected_dimensions) {
      var id_name = global.dashboard.selected_dimensions[id];
      select_el.append('<option class="filter_data_option_list__plot_dimension" value="' + id_name + '">Plot Dimensions | ' + id_name + '</option>');
    }
    // filter dimensions
    for (id in global.dashboard.filter_dimensions) {
      var id_name = global.dashboard.filter_dimensions[id];
      select_el.append('<option class="filter_data_option_list__filter_dimension" value="' + id_name + '">Filter Dimension | ' + id_name + '</option>');
    }
    // build new select
    select_el.val("default");
    //select_el.formSelect();
    select_el.formSelect();
    // clear filter data body
    $("#filter_data_body").html("");
    // do not display options
    $("#filter_data_options a").css("display", "none");
  });
  // click on element in filter data dropdown
  $("#data_dropdown_filter_data").on("click", "li", function() {
    var id_name_arr = $(this).attr('id').split("__");
    var id_name = id_name_arr[id_name_arr.length - 1];
    $(this).remove();
    $(this).api.dashboard.remove_filter_option(id_name);
    // update badge of dropdown
    $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
    M.toast({
      html: "Filter Options: " + id_name + " removed",
      displayLength: 4000
    });
    $("#tab_filter_data").trigger("click");
  });
  // dashboard layout
  $("#data_dropdown_layout_options").on("click", "li", function() {
    var timestamp = $(this).data("timestamp");
    var title = $(this).data("title");
    $(this).remove();
    $(this).api.dashboard.remove_layout_option(timestamp);
    // update badge of dropdown
    $("#dropdown_layout_options span").html($("#data_dropdown_layout_options li").length);
    M.toast({
      html: "Layout: " + title + " removed",
      displayLength: 4000
    });
    $(this).api.helper.dashboard_layout_clear();
  });
  // select filter dimension
  $("#filter_data_select").change(function() {
    var id_name = $(this).val();
    var request_options = {
      "first_run": true
    };
    $(this).api.filter_data(id_name, request_options);
    // display options: save, undo, delete from list(only if selection is filter dimension)
    var filter_type = $(this).find('option:selected').text().split(" ")[0];
    $("#filter_data_options a").css("display", "inline-block");
    if (filter_type == "Plot") {
      $("#filter_data_options_remove").css("display", "none");
    }
    let id_type = $(this).api.helper.id_type(id_name);
    if (id_type != "keyword") {
      $("#filter_data_options_select_all").css("display", "none");
    }
  });

  // plot dashboard
  $("#plot_dashboard").click(function() {
    if (global.dashboard.layout_options.length > 0) {
      $(this).api.plot_dashboard();
      if ($("#icon_prefix").val() == "") {
        $("#icon_prefix").val("Custom Dashboard Title");
      }
    } else {
      $("#plot_dashboard_body").fadeTo(0, 0).children().remove();
      $("#icon_prefix").val("");
      M.toast({
        html: "Please select at least one plot",
        displayLength: 4000
      });
    }
  });

  // update plot dashboard body after resizing window
  $(window).resize(function() {
    var window_height = $(window).height();
    var nav_height = $("nav").height();
    var footer_height = $("footer").height();
    $("#plot_dashboard_body_container").css("height", window_height - nav_height - footer_height).css("margin-top", nav_height);
    $("#plot_dashboard_body,#plot_dashboard_body_back").css("height", window_height - nav_height - footer_height);
  });
  // initial call of resize
  $(window).trigger("resize");
  // create dimension table
  $(this).api.select_dimensions();
  // load data from GET into gloabl enviroment
  let searchParams = new URLSearchParams(window.location.search);
  if (searchParams.has('data')) {
    let data = searchParams.get('data');
    data = JSON.parse(LZString.decompressFromEncodedURIComponent(data));
    console.log('Got data, try to parse');
    if (data) {
      global.dashboard.selected_dimensions = data.selected_dimensions;
      global.dashboard.filter_dimensions = data.filter_dimensions;
      global.dashboard.filter_options = data.filter_options;
      global.dashboard.layout_options = data.layout_options;
      global.dashboard.title = data.title;
      // update title
      $('#icon_prefix').val(data.title);
      // update selected dimension dropdown
      for (let i = 0; i < data.selected_dimensions.length; i++) {
        let id_name = data.selected_dimensions[i];
        let li = '<li id="data_dropdown_selected_dimensions__' + id_name + '"><a class="blue-grey-text text-lighten-2">' + id_name + '<i class="material-icons blue-text text-lighten-2">delete</i></a></li>';
        $("#data_dropdown_selected_dimensions").append(li);
        // update badge of dropdown
        $("#dropdown_selected_dimensions span").html($("#data_dropdown_selected_dimensions li").length);
      }
      for (let i = 0; i < data.filter_options.length; i++) {
        let id_name = data.filter_options[i]['id'];
        let li = '<li id="dropdown_filter_data__' + id_name + '"><a class="blue-grey-text text-lighten-2">' + id_name + '<i class="material-icons light-green-text">delete</i></a></li>';
        $("#data_dropdown_filter_data").append(li);
        // update number of filters
        $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
      }
      for (let i = 0; i < data.layout_options.length; i++) {
        let layout_option = data.layout_options[i];
        // add to layout dropdown
        let li = '<li data-timestamp="' + layout_option.timestamp + '" data-title="' + layout_option.title + '"><a class="blue-grey-text text-lighten-2">' + layout_option.title + '<i class="material-icons orange-text">delete</i></a></li>';
        $("#data_dropdown_layout_options").append(li);
        // update number of filters
        $("#dropdown_layout_options span").html($("#data_dropdown_layout_options li").length);
      }
      console.log('Data loaded');
      // plot dashboard
      $.when(global.config.deferred).done(function(value) {
        $(this).api.plot_dashboard();
        $(this).off();
        window.history.pushState("", "", window.location.href.split("?")[0]);
      });
    } else {
      console.log("Error: Data couldn't be loaded");
    }
  }

  $("#header_title").change(function() {
    global.dashboard.title = $('#icon_prefix').val();
  });
  // add konami event
  $(this).event.konami();
  // dashboard layout
  $(this).api.dashboard_layout();
  // footer informations: status
  $(this).event.server_status();
  // footer informations: total number of datasets in database
  $(this).event.total_number_datasets();
  // delete dashboard: clear url and reload withour cache
  $("#trigger_clear_dashboard").click(function() {
    window.history.pushState("", "", window.location.href.split("?")[0]);
    window.location.reload(true);
  });
});
