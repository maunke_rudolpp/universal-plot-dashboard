// =============================================================================
// API
// progress: api function -> helper function -> listener function
// api.dashboard: helper function to remove special dashboard options
// =============================================================================
$.fn.extend({
  api: {
    dashboard: {
      remove_selected_dimension: function(id_name) {
        console.log("Remove selecteded dimension " + id_name);
        global.dashboard.selected_dimensions = global.dashboard.selected_dimensions.filter(e => e !== id_name);
        console.log("Updated selecteded dimensions: " + global.dashboard.selected_dimensions);
      },
      remove_layout_option: function(timestamp) {
        global.dashboard.layout_options = global.dashboard.layout_options.filter(e => e.timestamp !== timestamp);
      },
      remove_filter_option: function(id_name) {
        console.log("Remove filter option " + id_name);
        global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
        console.log("Updated filter options: " + global.dashboard.filter_options);
      },
      remove_filter_dimension: function(id_name) {
        console.log("Remove filter dimension " + id_name);
        global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(e => e !== id_name);
        console.log("Updated filter dimensions: " + global.dashboard.filter_dimensions);
      },
      get_filter_option: function(id_name) {
        return global.dashboard.filter_options.filter(e => e.id == id_name);
      }
    },
    listener: {
      // api.listener
      select_dimensions: function() {
        // click event plot dimension select
        $('#table-choose-dimension .choose_dimension_select').off().click(function() {
          // read id_name from div id, could change it to data-name, here ...
          // ... just a historical decision, in other cases jquery data call ...
          // ... is used
          var id = $(this).attr("id");
          var id_name_arr = id.split("__");
          var id_name = id_name_arr[id_name_arr.length - 1];
          // maximal 6 dimensions are selectable
          if ($("#dropdown_selected_dimensions span").html() < global.dashboard.max_dim_filter_number) {
            // get number of datasets
            var datasets_number = $(this).parent().parent().find("td:nth-child(5)").html();
            // add to dashboard object
            global.dashboard.selected_dimensions.push(id_name);
            console.log("Update Selected Dimensions: " + global.dashboard.selected_dimensions);
            // update tbody
            $(this).api.select_dimensions();
            // reseplot_dashboardt filter
            $(".reset").trigger("click");
            // append to selected dimensions
            var new_li = '<li id="data_dropdown_selected_dimensions__' + id_name + '"><a class="blue-grey-text text-lighten-2">' + id_name + '<i class="material-icons blue-text text-lighten-2">delete</i></a></li>';
            $("#data_dropdown_selected_dimensions").append(new_li);
            // update badge of dropdown
            $("#dropdown_selected_dimensions span").html($("#data_dropdown_selected_dimensions li").length);
            // update individual filter List
            $(this).api.dashboard.remove_filter_dimension(id_name);
            M.toast({
              html: 'Plot Dimensions: ' + id_name + ' added<br>Number Datasets: ' + datasets_number,
              displayLength: 6000
            });
            // if length of selected dimensions changed -> new badge
            if (!$('#dropdown_selected_dimensions span.new').length) {
              $('#dropdown_selected_dimensions span').toggleClass("new");
            }
          } else {
            M.toast({
              html: 'Plot Dimensions: Maximum of ' + global.dashboard.max_dim_filter_number + ' dimemsions reached',
              displayLength: 4000
            });
          }
        });
        // click event add filter dimension in select dimensions table
        $('#table-choose-dimension .choose_dimension_filter').off().click(function() {
          var id = $(this).attr("id");
          var id_name_arr = id.split("__");
          var id_name = id_name_arr[id_name_arr.length - 1];
          var filter_class = "filter_listed";
          var color_listed = global.colorscheme.select_dimensions.filter.listed;
          var color_unlisted = global.colorscheme.select_dimensions.filter.unlisted;
          if (!$(this).hasClass(filter_class)) {
            // add to dashboard object
            global.dashboard.filter_dimensions.push(id_name);
            console.log("Update Filter Dimensions: " + global.dashboard.filter_dimensions);
            M.toast({
              html: "Filter List: " + id_name + " added",
              displayLength: 4000
            });
            $(this).addClass(filter_class).removeClass(color_unlisted).addClass(color_listed);
          } else {
            $(this).removeClass(filter_class).removeClass(color_listed).addClass(color_unlisted);
            $(this).api.dashboard.remove_filter_dimension(id_name);
            M.toast({
              html: "Filter List: " + id_name + " removed",
              displayLength: 4000
            });
          }
        });
      },
      // api.listener
      filter_data: {
        // api.listener.filter_data
        numeric: function(id_name, data) {
          // colors
          var color_listed = global.colorscheme.select_dimensions.filter.listed;
          var color_unlisted = global.colorscheme.select_dimensions.filter.unlisted;
          // save Filter Options
          $("#filter_data_options_save").off().click(function() {
            // proof if filter is updated or new
            var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
            var filter_is_new = (filter_option_existing.length == 0) ? true : false;
            // delete old Filter Options of id_name
            $(this).api.dashboard.remove_filter_option(id_name);
            // get range
            var range_min = $("#filter_data_numeric_range_min").val();
            var range_max = $("#filter_data_numeric_range_max").val();
            // update Filter Options: only add if keywords array has values
            var id_type = $(this).api.helper.id_type(id_name);
            var interval = $("#filter_data_interval_form").find(":checked").next('span').html();
            var filter_option = {
              id: id_name,
              type: id_type,
              config: {
                min: range_min,
                max: range_max
              }
            };
            console.log("Filter Option: " + filter_option);
            // append id to dropdown filter if filter options length < max number
            if (global.dashboard.filter_options.length < global.dashboard.max_dim_filter_number) {
              if (filter_is_new) {
                var new_li = '<li id="dropdown_filter_data__' + id_name + '"><a class="blue-grey-text text-lighten-2">' + id_name + '<i class="material-icons light-green-text">delete</i></a></li>';
                $("#data_dropdown_filter_data").append(new_li);
                // update number of filters
                $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
                // new filter sign
                if (!$('#dropdown_filter_data span.new').length) {
                  $('#dropdown_filter_data span').toggleClass("new");
                }
              }

              // add filter to global filter options
              global.dashboard.filter_options.push(filter_option);
              console.log("Update Filter Options: " + global.dashboard.filter_options);
              // get number of datasets
              var number_datasets = 0;
              for (var i in data) {
                number_datasets += data[i][1];
              }
              M.toast({
                html: "Filter Options: " + id_name + " added<br>Number Datasets: " + number_datasets,
                displayLength: 4000
              });
            } else {
              M.toast({
                html: 'Filter Options: Maximum of ' + global.dashboard.max_dim_filter_number + ' filter reached',
                displayLength: 4000
              });
            }
            // relayout tab content of filter data
            $("#tab_filter_data").trigger("click");
          });
          // reset options
          $("#filter_data_options_undo").off().click(function() {
            var request_options = {
              "undo": true,
              "first_run": false
            };
            // rebuild filter data content of id_name
            $(this).api.filter_data(id_name, request_options);
          });
          // get type of id_name
          var id_type = $(this).api.helper.id_type(id_name);
          // update range button
          $("#filter_data_numeric_update").off().click(function() {
            // get range
            var range_min = $("#filter_data_numeric_range_min").val();
            var range_max = $("#filter_data_numeric_range_max").val();
            if (id_type == "Integer") {
              range_min = parseInt(range_min);
              range_max = parseInt(range_max);
            } else {
              range_min = parseFloat(range_min);
              range_max = parseFloat(range_max);
            }
            // type checking
            if (!$.isNumeric(range_min) || !$.isNumeric(range_max) || range_min >= range_max) {
              return;
            }
            // request option for ajax call
            var request_options = {
              "first_run": false,
              "update": true,
              "min": range_min,
              "max": range_max,
            };
            // after click update -> rebuild filter data of id_name ...
            // .. with new range
            $(this).api.filter_data(id_name, request_options);
          });
          // remove filter dimension from filter list
          $("#filter_data_options_remove").off().click(function() {
            $(this).api.dashboard.remove_filter_dimension(id_name);
            $(this).api.dashboard.remove_filter_option(id_name);
            $("#tab_filter_data").trigger("click");
            // remove active filter of id in table
            M.toast({
              html: "Filter List: " + id_name + " removed",
              displayLength: 4000
            });
            $("#choose_dimension_filter__" + id_name).removeClass(color_listed).addClass(color_unlisted).removeClass("filter_listed");
            // remove from dropdown filter
            $("#data_dropdown_filter_data li").filter(function(index) {
              return $(this).attr("id").split("__")[1] == id_name;
            }).remove();
            $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length).removeClass("new");
          });
        },
        // api.listener.filter_data
        geo: function(id_name, features, data) {
          // colors
          var color_listed = global.colorscheme.select_dimensions.filter.listed;
          var color_unlisted = global.colorscheme.select_dimensions.filter.unlisted;
          $("#filter_data_geo_form label").off().click(function() {});
          // save Filter Options
          $("#filter_data_options_save").off().click(function() {
            // proof if filter is updated or new
            var relation = $("#filter_data_geo_form").find(":checked").next('span').html();

            var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
            var filter_is_new = (filter_option_existing.length == 0) ? true : false;
            // delete old Filter Options of id_name
            $(this).api.dashboard.remove_filter_option(id_name);
            // get polygons
            var geoList = [];
            features.eachLayer(function(e) {
              if (e._latlngs) {
                geoList.push(e.toGeoJSON());
              }
            });
            // update Filter Optionss: only add if keywords array has values
            var id_type = $(this).api.helper.id_type(id_name);
            var filter_option = {
              id: id_name,
              type: id_type,
              config: {
                features: geoList,
                relation: relation
              }
            };
            // append id to dropdown filter
            if (global.dashboard.filter_options.length < global.dashboard.max_dim_filter_number) {
              if (filter_is_new) {
                var new_li = '<li id="dropdown_filter_data__' + id_name + '"><a class="blue-grey-text text-lighten-2">' + id_name + '<i class="material-icons light-green-text">delete</i></a></li>';
                $("#data_dropdown_filter_data").append(new_li);
                // update number of filters
                $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
                // new filter sign
                if (!$('#dropdown_filter_data span.new').length) {
                  $('#dropdown_filter_data span').toggleClass("new");
                }
              }
              // push filter option to global filter options
              global.dashboard.filter_options.push(filter_option);
              console.log("Update Filter Options: " + global.dashboard.filter_options);
              // get number of datasets
              $(this).event.progress(true);
              // get number of datasets after setting this new filter
              $.ajax({
                type: 'POST',
                url: "/_filter_data",
                data: JSON.stringify({
                  "dimension": id_name,
                  "selected_dimensions": global.dashboard.selected_dimensions,
                  "filter_options": global.dashboard.filter_options,
                  "request_options": filter_option
                }),
                dataType: 'json',
                contentType: 'application/json',
                timeout: global.config.ajax_timeout
              }).done(function(table) {
                var data = table.data;
                var number_datasets = data;
                M.toast({
                  html: "Filter Options: " + id_name + " added<br>Number Datasets: " + number_datasets,
                  displayLength: 4000
                });

                $(this).event.progress(false);
              }).fail(function() {
                M.toast({
                  html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
                  displayLength: 4000
                });
                $(this).event.progress(false);
                console.log("Failed.");
              });
            } else {
              M.toast({
                html: 'Filter Options: Maximum of ' + global.dashboard.max_dim_filter_number + ' filter reached',
                displayLength: 4000
              });
            }
            // relayout filter data
            $("#tab_filter_data").trigger("click");
          });
          // reset options
          $("#filter_data_options_undo").off().click(function() {
            $("#filter_data_geo_form label").each(function() {
              if ($(this).find("span").html() == 'intersects') {
                $(this).find("input").attr('checked', true);
              } else {
                $(this).find("input").attr('checked', false);
              }
            });
            features.clearLayers();
          });
          // remove filter dimension from filter list
          $("#filter_data_options_remove").off().click(function() {
            $(this).api.dashboard.remove_filter_dimension(id_name);
            $(this).api.dashboard.remove_filter_option(id_name);
            $("#tab_filter_data").trigger("click");
            // remove active filter of id in table
            M.toast({
              html: "Filter List: " + id_name + " removed",
              displayLength: 4000
            });
            $("#choose_dimension_filter__" + id_name).removeClass(color_listed).addClass(color_unlisted).removeClass("filter_listed");
            // remove from dropdown filter
            $("#data_dropdown_filter_data li").filter(function(index) {
              return $(this).attr("id").split("__")[1] == id_name;
            }).remove();
            $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length).removeClass("new");
          });
        },
        // api.listener.filter_data
        date: function(id_name, initial_gte, initial_lte, data) {
          // colors
          var color_listed = global.colorscheme.select_dimensions.filter.listed;
          var color_unlisted = global.colorscheme.select_dimensions.filter.unlisted;
          // change group by option: year, month, day
          $("#filter_data_interval_form label").off().click(function() {
            var interval = $(this).find("span").html();
            var request_options = {
              "interval": interval
            };
            $(this).api.filter_data(id_name, request_options);
          });
          var relayout_event_handler = true;
          // date histogram relayout listener: set new x range
          $("#filter_data_date").off().get(0).on('plotly_relayout',
            function(eventdata) {
              if (relayout_event_handler) {
                // interval
                var interval = $("#filter_data_interval_form").find(":checked").next('span').html();
                var date_min;
                var date_max;
                if (!(eventdata['xaxis.range[0]'] == null)) {
                  date_min = eventdata['xaxis.range[0]'];
                } else {
                  date_min = $("#filter_data_date").get(0).layout.xaxis.range[0];
                }
                if (!(eventdata['xaxis.range[1]'] == null)) {
                  date_max = eventdata['xaxis.range[1]'];
                } else {
                  date_max = $("#filter_data_date").get(0).layout.xaxis.range[1];
                }
                // round date
                // update date range
                var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
                var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
                var date_min_rounded = new Date(date_min_html).getTime();
                var date_max_rounded = new Date(date_max_html).getTime();
                $('#filter_data_date_range_min').html(date_min_html).data("date", date_min_rounded);
                $('#filter_data_date_range_max').html(date_max_html).data("date", date_max_rounded);
                // round date down to interval level
                //alert(new Date(date_min_html));
                if (date_min_rounded != date_min && date_max_rounded != date_max) {
                  var update = {
                    'xaxis.range': [date_min_rounded, date_max_rounded]
                  };
                  Plotly.relayout($("#filter_data_date")[0], update);
                  relayout_event_handler = false;
                }
              } else {
                relayout_event_handler = true;
              }
            });
          // save Filter Options
          $("#filter_data_options_save").off().click(function() {
            // proof if filter is updated or new
            var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
            var filter_is_new = (filter_option_existing.length == 0) ? true : false;
            // delete old Filter Options of id_name
            $(this).api.dashboard.remove_filter_option(id_name);
            // get selected keywords
            var keywords = [];
            var counts = [];
            $(".filter_data_select_td").each(function() {
              if ($(this).data("listed")) {
                keywords.push($(this).data("value"));
                counts.push($(this).data("count"));
              }
            });
            // update Filter Optionss: only add if keywords array has values
            var id_type = $(this).api.helper.id_type(id_name);
            var interval = $("#filter_data_interval_form").find(":checked").next('span').html();
            var filter_option = {
              id: id_name,
              type: id_type,
              config: {
                gte: $('#filter_data_date_range_min').data("date"),
                lte: $('#filter_data_date_range_max').data("date"),
                interval: interval
              }
            };
            console.log("Filter: " + filter_option);
            if (global.dashboard.filter_options.length < global.dashboard.max_dim_filter_number) {
              // check if set ranges are new
              var non_empty_filter_option = true;
              if (filter_option.config.gte == initial_gte && filter_option.config.lte == initial_lte) {
                non_empty_filter_option = false;
              }
              if (non_empty_filter_option) {
                // append id to dropdown filter
                if (filter_is_new) {
                  var new_li = '<li id="dropdown_filter_data__' + id_name + '"><a class="blue-grey-text text-lighten-2">' + id_name + '<i class="material-icons light-green-text">delete</i></a></li>';
                  $("#data_dropdown_filter_data").append(new_li);
                  // update number of filters
                  $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
                  // new filter sign
                  if (!$('#dropdown_filter_data span.new').length) {
                    $('#dropdown_filter_data span').toggleClass("new");
                  }
                }
                global.dashboard.filter_options.push(filter_option);
                console.log("Update Filter Options: " + global.dashboard.filter_options);
                // get number of datasets
                var number_datasets = 0;
                for (var i in data) {
                  console.log(data[i][0] + " , " + Number(new Date(data[i][0])) + ", " + filter_option.config.gte + ", " + filter_option.config.lte);
                  var date_i = Number(new Date(data[i][0]));
                  if (date_i >= filter_option.config.gte && date_i < filter_option.config.lte) {
                    number_datasets += data[i][1];
                  }
                }
                M.toast({
                  html: "Filter Options: " + id_name + " added<br>Number Datasets: " + number_datasets,
                  displayLength: 4000
                });
              } else {
                // empty Filter Options -> delete dropdown element
                $("#dropdown_filter_data__" + id_name).remove();
                $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
                $('#dropdown_filter_data span').removeClass("new");
                M.toast({
                  html: "Filter Options: " + id_name + " has no constraints",
                  displayLength: 6000
                });
              }
            } else {
              M.toast({
                html: 'Filter Options: Maximum of ' + global.dashboard.max_dim_filter_number + ' filter reached',
                displayLength: 4000
              });
            }
            // relayout tab filter data
            $("#tab_filter_data").trigger("click");
          });
          // reset options
          $("#filter_data_options_undo").off().click(function() {
            var update = {
              'xaxis.range': [initial_gte, initial_lte]
            };
            Plotly.relayout($('#filter_data_date')[0], update);
            var date_min = $("#filter_data_date").get(0).layout.xaxis.range[0];
            var date_max = $("#filter_data_date").get(0).layout.xaxis.range[1];
            // update date range
            var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
            var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
          });
          // remove filter dimension from filter list
          $("#filter_data_options_remove").off().click(function() {
            $(this).api.dashboard.remove_filter_dimension(id_name);
            $(this).api.dashboard.remove_filter_option(id_name);
            $("#tab_filter_data").trigger("click");
            // remove active filter of id in table
            M.toast({
              html: "Filter List: " + id_name + " removed",
              displayLength: 4000
            });
            $("#choose_dimension_filter__" + id_name).removeClass(color_listed).addClass(color_unlisted).removeClass("filter_listed");
            // remove from dropdown filter
            $("#data_dropdown_filter_data li").filter(function(index) {
              return $(this).attr("id").split("__")[1] == id_name;
            }).remove();
            $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length).removeClass("new");
          });
        }
      }
    },
    helper: {
      // api.helper
      plot_dashboard: function(data) {
        // build container in dashboard body
        var number_tiles = global.dashboard.layout_options.length;
        // no_tiles cases
        if (number_tiles == 1) {
          $("#plot_dashboard_body_tile_0").css({
            "width": "100%",
            "height": "100%",
            "display": "inline-block"
          });
        } else if (number_tiles == 2) {
          $("#plot_dashboard_body_tile_0, #plot_dashboard_body_tile_1").css({
            "width": "50%",
            "height": "100%",
            "display": "inline-block"
          });
        } else if (number_tiles == 3) {
          $("#plot_dashboard_body_tile_0, #plot_dashboard_body_tile_1").css({
            "width": "50%",
            "height": "50%",
            "display": "inline-block"
          });
          $("#plot_dashboard_body_tile_2").css({
            "width": "100%",
            "height": "50%",
            "display": "inline-block"
          });
        } else if (number_tiles == 4) {
          $("#plot_dashboard_body_tile_0, #plot_dashboard_body_tile_1,#plot_dashboard_body_tile_2, #plot_dashboard_body_tile_3").css({
            "width": "50%",
            "height": "50%",
            "display": "inline-block"
          });
        } else if (number_tiles == 5) {
          $("#plot_dashboard_body_tile_0, #plot_dashboard_body_tile_1").css({
            "width": "50%",
            "height": "50%",
            "display": "inline-block"
          });
          $("#plot_dashboard_body_tile_2, #plot_dashboard_body_tile_3, #plot_dashboard_body_tile_4").css({
            "width": "33.3333333%",
            "height": "50%",
            "display": "inline-block"
          });
        } else if (number_tiles == 6) {
          $("#plot_dashboard_body").children().css({
            "width": "33.33333333%",
            "height": "50%",
            "display": "inline-block"
          });
        }
        // plot layout
        for (var id in data) {
          var layout_option = global.dashboard.layout_options[id];
          var plot_data = data[id];
          $(this).plot.dashboard[layout_option.option](id, layout_option, plot_data);
        }
      },
      // api.helper
      dashboard_layout_clear: function() {
        // clear content of tab layout options content
        var el_dim = $("#dashboard_layout_select_dimensions");
        el_dim.children().not(":disabled").remove();
        // plot dimensions
        var plot_dimensions = global.dashboard.selected_dimensions;
        for (id in plot_dimensions) {
          var id_name = plot_dimensions[id];
          var id_type = $(this).api.helper.id_type(id_name);
          el_dim.append('<option class="dashboard_layout_select_dimensions_option" value="' + id_name + '" data-id_type="' + id_type + '">' + id_name + ' (' + id_type + ')</option>');
        }
        // build new select
        el_dim.val("default");
        el_dim.formSelect();
        var el = $("#dashboard_layout_plot_options");
        el.children().not(":disabled").remove();
        el.val("default");
        el.formSelect();

        $("#dashboard_layout_plot_title").val('');
        $("#dashboard_layout_plot_num_scrolls").val(9999);
        $("#dashboard_layout_option_two_axis").fadeOut(0);
      },
      // api.helper
      plot_options_types_code: function(input) {
        var code = "";
        for (var i in input) {
          code += input[i];
        }
        return code;
      },
      // api.helper
      // map: dimensions -> available plot options
      plot_options: function(plot_dimensions) {
        // map: type of dimension(s) -> array of plot options
        var plot_options_map = ({
          single: {
            "geoshape": ["PolygonMap", "GeoHeatmap"],
            "date": ["Histogram", "PieChart"],
            "integer": ["Histogram", "PieChart"],
            "double": ["Histogram", "PieChart"],
            "keyword": ["Histogram", "PieChart"],
          },
          two: {
            // Geo
            "dategeoshape": ["CategoricalMap"],
            "doublegeoshape": ["CategoricalMap", "GeoNumHeatmap"],
            "geoshapeinteger": ["CategoricalMap", "GeoNumHeatmap"],
            "geoshapekeyword": ["CategoricalMap"],
            // Date
            "datekeyword": ["Scatter", "Heatmap", "StackedBarChart"],
            "dateinteger": ["Scatter", "Heatmap", "StackedBarChart"],
            "datedouble": ["Scatter", "Heatmap", "StackedBarChart"],
            "datedate": ["Scatter", "Heatmap", "StackedBarChart"],
            // Integer
            "integerkeyword": ["Scatter", "Heatmap", "StackedBarChart"],
            "integerinteger": ["Scatter", "Heatmap", "StackedBarChart"],
            "doubleinteger": ["Scatter", "Heatmap", "StackedBarChart"],
            // Double
            "doublekeyword": ["Scatter", "Heatmap", "StackedBarChart"],
            "doubledouble": ["Scatter", "Heatmap", "StackedBarChart"],
            // Keyword
            "keywordkeyword": ["Heatmap", "StackedBarChart"],
          }
        });
        // get types of dimensions
        var plot_types = [];
        for (var i in plot_dimensions) {
          var id_name = plot_dimensions[i];
          var id_type = $(this).api.helper.id_type(id_name);
          plot_types.push(id_type);
        }
        // fill available options into options array
        var plot_options_arr = [];
        // case single dimension
        if (plot_dimensions.length == 1) {
          plot_options_arr.push.apply(plot_options_arr, plot_options_map.single[plot_types[0]]);
        } else if (plot_dimensions.length == 2) {
          var types_code = $(this).api.helper.plot_options_types_code(plot_types.sort());
          console.log(types_code);
          plot_options_arr.push.apply(plot_options_arr, plot_options_map.two[types_code]);
        }
        return plot_options_arr;
      },
      // api.helper
      id_type: function(id_name) {
        return global.dashboard.dimensions_types.find(x => x.id == id_name).type;
      },
      // api.helper
      build_tablesorter: function(el) {
        var max_rows = 9999;
        if (!(el.hasClass('tablesorter'))) {
          el.tablesorter({
              theme: "materialize",
              widthFixed: false,
              widgets: ["filter", "zebra"],
              widgetOptions: {
                zebra: ["even", "odd"],
                filter_reset: ".reset",
              }
            })
            .tablesorterPager({
              size: max_rows,
              container: $(".ts-pager"),
              cssGoto: ".pagenum",
              removeRows: false,
              output: '{{startRow} - {endRow} / {filteredRows} ({totalRows})}'
            });
        }
      },
      // api.helper
      build_select_dimensions: function(data) {
        var table = $("#table-choose-dimension");
        // build tablesorter
        $(this).api.helper.build_tablesorter(table);
        // remove previous activated info tooltips
        table.find(".tooltipped").tooltip("destroy");
        // build tbody
        var tbody = "";
        for (var row in data) {
          var id_name = data[row][0];
          tbody += "<tr><td>";
          // information about description
          tbody += '<a style="margin-left:3px;margin-right:3px;" id="choose_dimension_info_row__' + id_name + '" class="choose_dimension_info grey lighten-5 btn-flat tooltipped" data-position="top" data-tooltip="Description: ' + data[row][5] + '"><i class="small material-icons blue-grey-text text-lighten-2">info_outline</i></a>';
          // add select/filter button
          tbody += '<a style="margin-left:3px;margin-right:3px;" id="choose_dimension_select__' + id_name + '" class="choose_dimension_select waves-effect waves-dark blue lighten-3 btn-small"><i class="small material-icons">add</i>Plot</a>';
          // "add to filter" after chosen at least one dimension
          if (global.dashboard.selected_dimensions.length > 0) {
            var color;
            var class_filter_listed;
            if ($.inArray(id_name, global.dashboard.filter_dimensions) != -1) {
              color = global.colorscheme.select_dimensions.filter.listed;
              class_filter_listed = "filter_listed";
            } else {
              color = global.colorscheme.select_dimensions.filter.unlisted;
              class_filter_listed = "";
            }
            tbody += '<a style="margin-left:3px;margin-right:3px;" id="choose_dimension_filter__' + id_name + '" class="choose_dimension_filter waves-effect waves-dark ' + color + ' btn-small ' + class_filter_listed + '"><i class="small material-icons">library_add</i>Filter</a>';
          }
          tbody += "</td>";
          for (var col in data[row]) {
            if (col != 5) {
              tbody += "<td>" + data[row][col] + "</td>";
            }
          }
          tbody += "</tr>";
        }
        // tbody into page
        $('#tbody-choose-dimension').html(tbody);
        // activate info tooltips
        table.find(".tooltipped").tooltip();
        // tablesorter update and fade in table
        table.trigger("update");
        $("#table-choose-dimension .tablesorter-header,#table-choose-dimension .tablesorter-filter-row td, #filter_data_body tfoot tr th").addClass("grey lighten-5");
        table.fadeIn(500);
        // remove filter dimension from filter list if not exist in table
        var delete_filter_dim = [];
        for (var id in global.dashboard.filter_dimensions) {
          var id_name = global.dashboard.filter_dimensions[id];
          var filter_id_in_table = false;
          if (global.dashboard.selected_dimensions.length > 0) {
            for (var row in global.dashboard.dimensions_table) {
              if (id_name == global.dashboard.dimensions_table[row][0]) {
                filter_id_in_table = true;
              }
            }
          }
          if (!filter_id_in_table) {
            delete_filter_dim.push(id_name);
          }
        }
        // allow only filter dimensions that are not a selected dimension
        global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(function(el) {
          return delete_filter_dim.indexOf(el) < 0;
        });
        $("#tab_filter_data").trigger("click");
        $(this).api.listener.select_dimensions();
      },
      // api.helper
      // build array of (id_name,id_types) tuples for lookup table
      data2dimensiontype: function(data) {
        for (var row in data) {
          var dimension_type = {
            "id": data[row][0],
            "type": data[row][2]
          };
          global.dashboard.dimensions_types.push(dimension_type);
        }
      },
      // api.helper
      build_filter_categorical: function(id_name, data) {
        console.log("Build filter categorial...");
        // undbind jquery listener and remove all elements in container
        $("#filter_data_body").find("*").off();
        $("#filter_data_body").find("*").remove();
        // unlisten undo, save, select_all and remove button
        $("#filter_data_options_undo,#filter_data_options_remove,#filter_data_options_select_all,#filter_data_options_save").off();
        var filter_data_body = '\
                  <table id="filter_data_table" class="grey-text text-darken-3" style="margin-bottom:48px;">\
                    <thead>\
                      <tr>\
                        <th>Select</th>\
                        <th>Value</th>\
                        <th>Count</th>\
                      </tr>\
                    </thead>\
                    <tfoot>\
                      <tr>\
                      <th>Select</th>\
                      <th>Value</th>\
                      <th>Count</th>\
                      </tr>\
                        <tr class="tablesorter-ignoreRow" style="display:none;">\
                        <th colspan="3" class="ts-pager form-horizontal">\
                          <button type="button" class="btn first"><i class="small material-icons">first_page</i></button>\
                          <button type="button" class="btn prev"><i class="small material-icons">navigate_before</i></button>\
                          <span class="pagedisplay"></span>\
                          <!-- this can be any element, including an input -->\
                          <button type="button" class="btn next"><i class="small material-icons">navigate_next</i></button>\
                          <button type="button" class="btn last"><i class="small material-icons">last_page</i></button>\
                          <select class="pagesize browser-default" title="Select page size">\
                            <option value="10">10</option>\
                            <option value="20">20</option>\
                            <option value="30">30</option>\
                            <option value="40">40</option>\
                            <option selected="selected" value="9999">9999</option>\
                          </select>\
                          <select class="pagenum browser-default" title="Select page number"></select>\
                        </th>\
                      </tr>\
                    </tfoot>\
                    <tbody id="filter_data_tbody">\
                    </tbody>\
                  </table>';
        $("#filter_data_body").html(filter_data_body);
        // add select, id, count to tbody
        var tbody = "";
        for (var row in data) {
          var id_value = data[row][0];
          var id_count = data[row][1];
          tbody += "<tr>";
          tbody += '<td><a style="margin-left:3px;margin-right:3px;" data-value="' + id_value + '" data-count="' + id_count + '" data-listed=false class="filter_data_select_td waves-effect waves-dark blue-grey lighten-3 btn-small"><i class="small material-icons">add</i>Add</a><span style="display: none;">1</span></td>';
          for (var col in data[row]) {
            tbody += "<td>" + data[row][col] + "</td>";
          }
          tbody += "</tr>";
          if (row >= 998) {
            M.toast({
              html: 'Filter Options: maximal numbers of 999 displayed unique values reached ',
              displayLength: 6000
            });
          }
        }
        $("#filter_data_tbody").html(tbody);
        $(this).api.helper.build_tablesorter($('#filter_data_body table'));

        $("#filter_data_body .tablesorter-header,#filter_data_body .tablesorter-filter-row td, #filter_data_body tfoot tr th").addClass("grey lighten-5");
        // colors
        var color_listed = global.colorscheme.select_dimensions.filter.listed;
        var color_unlisted = global.colorscheme.select_dimensions.filter.unlisted;
        // colorize items selected in Filter Options
        var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
        var filter_is_new = true;
        if (filter_option_existing.length > 0) {
          var existing_keywords = filter_option_existing[0].config.keywords;
          for (var i in existing_keywords) {
            var keyword = existing_keywords[i];
            $(".filter_data_select_td").each(function() {
              if ($(this).data("value") == keyword) {
                $(this).next('span')[0].innerHTML = '0';
                $(this).data("listed", true);
                $(this).removeClass(color_unlisted);
                $(this).addClass(color_listed);
              }
            })
          }
          // update tablesorter listener (search, sort in columns)
          $("#filter_data_table").trigger("update");
        }
        // add value
        $('#filter_data_body table').on("click", '.filter_data_select_td', function() {
          var id_value = $(this).data("value");
          if ($(this).data("listed")) {
            $(this).next('span')[0].innerHTML = '1';
            $(this).data("listed", false);
            $(this).removeClass(color_listed);
            $(this).addClass(color_unlisted);
          } else {
            $(this).next('span')[0].innerHTML = '0';
            $(this).data("listed", true);
            $(this).removeClass(color_unlisted);
            $(this).addClass(color_listed);
          }
          $("#filter_data_table").trigger("update");
        });
        //select all options
        $("#filter_data_options_select_all").off().click(function() {
          $(".filter_data_select_td").each(function() {
            $(this).data("listed", true);
            $(this).removeClass(color_unlisted);
            $(this).addClass(color_listed);
          });
        });
        // reset options
        $("#filter_data_options_undo").off().click(function() {
          $(".filter_data_select_td").each(function() {
            if ($(this).data("listed")) {
              $(this).next('span')[0].innerHTML = '1';
            }
          });
          $("#filter_data_table").trigger("update");
          var select_items = $("#filter_data_body table .filter_data_select_td");
          select_items.data("listed", false);
          select_items.removeClass(color_listed);
          select_items.addClass(color_unlisted);
        });
        // remove filter dimension from filter list
        $("#filter_data_options_remove").off().click(function() {
          $(this).api.dashboard.remove_filter_dimension(id_name);
          $(this).api.dashboard.remove_filter_option(id_name);
          $("#tab_filter_data").trigger("click");
          // remove active filter of id in table
          M.toast({
            html: "Filter List: " + id_name + " removed",
            displayLength: 4000
          });
          $("#choose_dimension_filter__" + id_name).removeClass(color_listed).addClass(color_unlisted).removeClass("filter_listed");
          // remove from dropdown filter
          $("#data_dropdown_filter_data li").filter(function(index) {
            return $(this).attr("id").split("__")[1] == id_name;
          }).remove();
          $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length).removeClass("new");
        });
        // save Filter Options
        $("#filter_data_options_save").off().click(function() {
          // proof if filter is updated or new
          var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
          var filter_is_new = (filter_option_existing.length == 0) ? true : false;
          // delete old Filter Options of id_name
          $(this).api.dashboard.remove_filter_option(id_name);
          // get selected keywords
          var keywords = [];
          var counts = [];
          $(".filter_data_select_td").each(function() {
            if ($(this).data("listed")) {
              keywords.push($(this).data("value"));
              counts.push($(this).data("count"));
            }
          });
          // update Filter Optionss: only add if keywords array has values
          var id_type = $(this).api.helper.id_type(id_name);
          var filter_option = {
            id: id_name,
            type: id_type,
            config: {
              keywords: keywords
            }
          };
          // only save filter if more than zero keywords in table are added
          if (keywords.length > 0) {
            // append id to dropdown filter
            if (filter_is_new) {
              var new_li = '<li id="dropdown_filter_data__' + id_name + '"><a class="blue-grey-text text-lighten-2">' + id_name + '<i class="material-icons light-green-text">delete</i></a></li>';
              $("#data_dropdown_filter_data").append(new_li);
              // update number of filters
              $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
              // new filter sign
              if (!$('#dropdown_filter_data span.new').length) {
                $('#dropdown_filter_data span').toggleClass("new");
              }
            }
            global.dashboard.filter_options.push(filter_option);
            console.log("Update Filter Options: " + global.dashboard.filter_options);
            var number_datasets = counts.reduce(function(pv, cv) {
              return pv + cv;
            }, 0);
            M.toast({
              html: "Filter Options: " + id_name + " added<br>Number Datasets: " + number_datasets,
              displayLength: 4000
            });
          } else {
            // empty Filter Options -> delete dropdown element
            $("#dropdown_filter_data__" + id_name).remove();
            $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);

            $('#dropdown_filter_data span').removeClass("new");
            M.toast({
              html: "Filter Options: " + id_name + " has no constraints",
              displayLength: 6000
            });
          }
          $("#tab_filter_data").trigger("click");
        });
        // body fade in
        $("#filter_data_body").fadeIn(500);
        console.log("Build filter data body done.");
      },
      // api.helper
      filter_data_date_parse_html: function(date_value, interval) {
        var date = new Date(date_value);
        var date_span;
        if (interval == "day") {
          date_span = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
        } else if (interval == "month") {
          date_span = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + "01";
        } else if (interval == "year") {
          date_span = date.getFullYear() + "-" + "01" + "-" + "01";
        }
        return date_span;
      },
      // api.helper
      filter_data_date_xrange_adjust: function(xrange) {
        var xrange_min = xrange[0];
        var xrange_max = new Date(xrange[1]);
        xrange_max.setFullYear(xrange_max.getFullYear() + 1);
        xrange_max.setMonth(0);
        xrange_max.setDate(1);
        return [xrange_min, xrange_max.getTime()]
      },
      // api.helper
      build_filter_date: function(id_name, data, request_options) {
        // check for existing filter
        var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
        if (filter_option_existing.length > 0) {
          var option_interval = filter_option_existing[0].config.interval;
          if (request_options.interval != option_interval && request_options.hasOwnProperty("first_run")) {
            var request_options = {
              "interval": option_interval
            };
            $(this).api.filter_data(id_name, request_options);
            return;
          }
        }
        $("#filter_data_body").find("*").off();
        $("#filter_data_body").find("*").remove();
        var interval = request_options.interval;
        // build content divs
        var content = '\
        <div class="row" style="margin-bottom:0px;">\
          <div class="col s6 blue-grey-text text-lighten-2">\
            <i style="margin-right:10px;" class="material-icons">date_range</i>\
            <span id="filter_data_date_range_min"></span>\
            <i class="material-icons">chevron_right</i>\
            <span id="filter_data_date_range_max"></span>\
          </div>\
          <div class="col s6 right-align blue-grey-text text-lighten-2">\
            <form id="filter_data_interval_form" style="margin:0;padding:0;" action="#">\
              <label>\
                <input name="interval" type="radio" />\
                <span>year</span>\
              </label>\
              <label>\
                <input name="interval" type="radio" />\
                <span>month</span>\
              </label>\
              <label>\
                <input name="interval" type="radio" />\
                <span>day</span>\
              </label>\
            </form>\
          </div>\
          </div><div id="filter_data_date"></div>';
        $("#filter_data_body").html(content);
        $("#filter_data_body form label").each(function() {
          if ($(this).find("span").html() == interval) {
            $(this).find("input").attr('checked', true);
          } else {
            $(this).find("input").attr('checked', false);
          }
        });
        var modal_height = $("#modal-custom-dashboard").height();
        var modal_width = $("#modal-custom-dashboard").width();
        var plot_div = $("#filter_data_date");
        plot_div.css("width", modal_width);
        plot_div.css("height", modal_height - 250);
        // $('#filter_data_date_start').datepicker({
        //   container: 'body'
        // });
        $(this).plot.filter_data.date(id_name, data, interval);
        var range_initial = $(this).api.helper.filter_data_date_xrange_adjust(plot_div.get(0).layout.xaxis.range);

        // zoom in, if filter option activated
        var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
        if (filter_option_existing.length > 0) {
          var option_gte = filter_option_existing[0].config.gte;
          var option_lte = filter_option_existing[0].config.lte;
          var update = {
            'xaxis.range': [option_gte, option_lte]
          };
          Plotly.relayout(plot_div[0], update);
          date_min = plot_div.get(0).layout.xaxis.range[0];
          date_max = plot_div.get(0).layout.xaxis.range[1];
          // update date range
          var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
          var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
          $('#filter_data_date_range_min').html(date_min_html).data('date', date_min);
          $('#filter_data_date_range_max').html(date_max_html).data('date', date_max);
        } else {
          date_min = plot_div.get(0).layout.xaxis.range[0];
          date_max = plot_div.get(0).layout.xaxis.range[1];
          var update = {
            'xaxis.range': $(this).api.helper.filter_data_date_xrange_adjust([date_min, date_max])
          };
          Plotly.relayout(plot_div[0], update);
          date_min = plot_div.get(0).layout.xaxis.range[0];
          date_max = plot_div.get(0).layout.xaxis.range[1];
          var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
          var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
          $('#filter_data_date_range_min').html(date_min_html).data('date', date_min);
          $('#filter_data_date_range_max').html(date_max_html).data('date', date_max);
        }
        $(this).api.listener.filter_data.date(id_name, range_initial[0], range_initial[1], data);
        $("#filter_data_body").fadeIn(500);
      },
      // api.helper
      build_filter_numeric: function(id_name, data, request_options) {
        $("#filter_data_body").find("*").off();
        $("#filter_data_body").find("*").remove();
        var interval = request_options.interval;
        var content = '\
        <div class="row" style="margin-bottom:0px;">\
          <div class="col s6">\
            <div class="input-field col s3">\
              <input id="filter_data_numeric_range_min" type="number" step=any class="validate blue-grey-text text-lighten-2">\
              <label for="filter_data_numeric_range_min" class="blue-grey-text text-lighten-2">Min. Value</label>\
            </div>\
            <div class="input-field col s3">\
              <input id="filter_data_numeric_range_max" type="number" step=any class="validate blue-grey-text text-lighten-2">\
              <label for="filter_data_numeric_range_max" class="blue-grey-text text-lighten-2">Max. Value</label>\
            </div>\
          <button id="filter_data_numeric_update" style="margin-left:15px;margin-top:20px;" type="button" class="btn-small first waves-effect waves-dark blue-grey lighten-2"><i class="small material-icons">autorenew</i>Update</button>\
          </div>\
          <div class="col s6 right-align">\
          </div>\
          </div><div id="filter_data_numeric"></div>';
        $("#filter_data_body").html(content);

        var modal_height = $("#modal-custom-dashboard").height();
        var modal_width = $("#modal-custom-dashboard").width();
        var plot_div = $("#filter_data_numeric");
        plot_div.css("width", modal_width);
        plot_div.css("height", modal_height - 300);
        $(this).plot.filter_data.numeric(id_name, data);

        // update input fields
        var plot_data = plot_div.get(0).data;
        var plot_data_x = plot_data[0].x;
        var plot_range_min = Math.min(...plot_data_x);
        var plot_range_max = Math.max(...plot_data_x);
        $("#filter_data_numeric_range_min").val(plot_range_min);
        $("#filter_data_numeric_range_max").val(plot_range_max);
        M.updateTextFields();

        $(this).api.listener.filter_data.numeric(id_name, data);

        $("#filter_data_body").fadeIn(500);
      },
      // api.helper
      build_filter_geo: function(id_name, data, request_options) {
        // check for existing filter
        var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
        var relation = "intersects";
        if (filter_option_existing.length > 0) {
          var relation = filter_option_existing[0].config.relation;
        }
        $("#filter_data_body").find("*").off();
        $("#filter_data_body").find("*").remove();
        var content = '\
        <div class="col s6 left-align">\
          <form id="filter_data_geo_form" style="margin:0;padding:0;" action="#">\
            <label>\
              <input name="relation" type="radio" />\
              <span>intersects</span>\
            </label>\
            <label>\
              <input name="relation" type="radio" />\
              <span>within</span>\
            </label>\
            <label>\
              <input name="relation" type="radio" />\
              <span>contains</span>\
            </label>\
          </form>\
        </div>\
        <div id="filter_data_geo"></div>';
        $("#filter_data_body").html(content);
        $("#filter_data_geo_form label").each(function() {
          if ($(this).find("span").html() == relation) {
            $(this).find("input").attr('checked', true);
          } else {
            $(this).find("input").attr('checked', false);
          }
        });
        var modal_height = $("#modal-custom-dashboard").height();
        var modal_width = $("#modal-custom-dashboard").width();
        var plot_div = $("#filter_data_geo");
        plot_div.css("width", modal_width - 50);
        plot_div.css("height", modal_height - 250);
        // needs to make visible befor map init
        $("#filter_data_body").fadeIn(500);
        let featureGroup = $(this).plot.filter_data.geo(id_name, request_options.features);
        $(this).api.listener.filter_data.geo(id_name, featureGroup, data);
      }
    },
    // api
    select_dimensions: function() {
      console.log("Loading select dimensions table...");
      $(this).event.progress(true);
      $('#table-choose-dimension').fadeOut(0);
      global.ajax.select_dimensions = $.ajax({
        type: 'POST',
        url: "/_select_dimensions",
        data: JSON.stringify({
          "selected_dimensions": global.dashboard.selected_dimensions
        }),
        dataType: 'json',
        contentType: 'application/json',
        beforeSend: function() {
          if (!global.ajax.select_dimensions && global.ajax.select_dimensions.readyState < 4) {
            global.ajax.select_dimensions.abort();
          }
        },
        complete: function(response) {
          global.config.deferred.resolve("Done");
        },
        timeout: global.config.ajax_timeout
      }).done(function(table) {
        var data = table.data;
        // save dimension types
        if (global.dashboard.dimensions_types.length == 0) {
          $(this).api.helper.data2dimensiontype(data);
        }
        // recent dimensions_select table
        global.dashboard.dimensions_table = data;
        $(this).api.helper.build_select_dimensions(data);
        $(this).event.progress(false);
        console.log("Done.");
      }).fail(function() {
        M.toast({
          html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
          displayLength: 4000
        });
        $(this).event.progress(false);
      });
    },
    // api
    filter_data: function(id_name, request_options) {
      $(this).event.progress(true);
      var id_type = $(this).api.helper.id_type(id_name);
      console.log("Loading filter data for id " + id_name + " with type " + id_type + " ...");
      if (request_options.first_run) {
        switch (id_type) {
          case "date":
            request_options['interval'] = "year";
            break;
          case "integer":
            var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
            if (filter_option_existing.length > 0) {
              var config = filter_option_existing[0].config;
              request_options['min'] = config.min;
              request_options['max'] = config.max;
              request_options.first_run = false;
            }
            break;
          case "double":
            var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
            if (filter_option_existing.length > 0) {
              var config = filter_option_existing[0].config;
              request_options['min'] = config.min;
              request_options['max'] = config.max;
              request_options.first_run = false;
            }
            break;
          case "geoshape":
            var filter_option_existing = $(this).api.dashboard.get_filter_option(id_name);
            if (filter_option_existing.length > 0) {
              var config = filter_option_existing[0].config;
              request_options['features'] = config.features;
              request_options['relation'] = config.relation;
              request_options.first_run = false;
            }
            break;
        }
      }
      console.log("Sending to server: " + JSON.stringify({
        "dimension": id_name,
        "selected_dimensions": global.dashboard.selected_dimensions,
        "filter_options": global.dashboard.filter_options,
        "request_options": request_options
      }));
      $("#filter_data_body").fadeOut(0);
      $.ajax({
        type: 'POST',
        url: "/_filter_data",
        data: JSON.stringify({
          "dimension": id_name,
          "selected_dimensions": global.dashboard.selected_dimensions,
          "filter_options": global.dashboard.filter_options,
          "request_options": request_options
        }),
        dataType: 'json',
        contentType: 'application/json',
        timeout: global.config.ajax_timeout
      }).done(function(table) {
        var data = table.data;
        switch (id_type) {
          case "keyword":
            $(this).api.helper.build_filter_categorical(id_name, data);
            break;
          case "date":
            $(this).api.helper.build_filter_date(id_name, data, request_options);
            break;
          case "integer":
            $(this).api.helper.build_filter_numeric(id_name, data, request_options);
            break;
          case "double":
            $(this).api.helper.build_filter_numeric(id_name, data, request_options);
            break;
          case "geoshape":
            $(this).api.helper.build_filter_geo(id_name, data, request_options);
            break;
        }
        $(this).event.progress(false);
      }).fail(function() {
        M.toast({
          html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
          displayLength: 4000
        });
        $(this).event.progress(false);
        console.log("Failed.");
      });
    },
    // api
    dashboard_layout: function() {
      $("#dashboard_layout_option_two_axis").fadeOut(0);
      // tab plot layout
      // on click dropdown selection dimensions & modal header tab_filter_data
      $("#tab_dashboard_layout").off().click(function() {
        var plot_dimensions = global.dashboard.selected_dimensions;
        if (plot_dimensions.length == 0) {
          M.toast({
            html: 'Please select at least one plot dimension',
            displayLength: 4000
          });
        } else {
          var el = $("#dashboard_layout_select_dimensions");
          el.children().not(":disabled").remove();
          // plot dimensions
          for (id in plot_dimensions) {
            var id_name = plot_dimensions[id];
            var id_type = $(this).api.helper.id_type(id_name);
            el.append('<option class="dashboard_layout_select_dimensions_option" value="' + id_name + '" data-id_type="' + id_type + '">' + id_name + ' (' + id_type + ')</option>');
          }
          // build new select
          el.val("default");
          el.formSelect();
        }
      });
      $("#dashboard_layout_plot_options").off().change(function() {
        $("#dashboard_layout_plot_title").val($(this).val()).focus();
        var dims = $("#dashboard_layout_select_dimensions").val();
        var opt = $("#dashboard_layout_plot_options").val();
        if (dims.length == 1 && opt == "PieChart") {
          $("#dashboard_layout_plot_title").val($(this).val() + " of " + dims[0]);
        } else if (dims.length == 2 && opt == "StackedBarChart") {
          var axis_2 = $("#dashboard_layout_axis_2 span").html();
          $("#dashboard_layout_plot_title").val($(this).val() + " of " + axis_2);
        } else if (dims.length == 2 && (opt.search("Geo") >= 0 || opt.search("Categorical") >= 0)) {
          var plot_dimensions = $("#dashboard_layout_select_dimensions").val();
          for (var i in plot_dimensions) {
            var dim = plot_dimensions[i];
            if ($(this).api.helper.id_type(dim) != "GeoObject") {
              $("#dashboard_layout_plot_title").val(opt + " of " + dim);
            }
          }
        }

      });
      $("#dashboard_layout_select_dimensions").off().change(function() {
        // clear title
        $("#dashboard_layout_plot_title").val("");
        // get selected plot dimensions
        var plot_dimensions = $("#dashboard_layout_select_dimensions").val();
        if (plot_dimensions.length > 2) {
          M.toast({
            html: 'Choose maximal two dimensions for the plot',
            displayLength: 4000
          });
        } else if (plot_dimensions.length == 0) {
          M.toast({
            html: 'Choose at least one dimension for the plot',
            displayLength: 4000
          });
        } else {
          var plot_options = $(this).api.helper.plot_options(plot_dimensions);
          var el = $("#dashboard_layout_plot_options");
          el.children().not(":disabled").remove();
          // plot dimensions
          for (id in plot_options) {
            var option = plot_options[id];
            el.append('<option class="dashboard_layout_plot_options_option" value="' + option + '">' + option + '</option>');
          }
          // build new select
          el.val("default");
          el.formSelect();
          // if two dimensions chosen -> give option to swap axis
          if (plot_dimensions.length == 2 && (plot_options[0].search("Geo") < 0 && plot_options[0].search("Categorical") < 0)) {
            // write dimensions to axis flat buttons (left 1/right 2)
            $("#dashboard_layout_axis_1 span").html(plot_dimensions[0]);
            $("#dashboard_layout_axis_2 span").html(plot_dimensions[1]);
            $("#dashboard_layout_option_two_axis").fadeIn(500);
          } else {
            $("#dashboard_layout_option_two_axis").fadeOut(0);
          }
        }
      });
      $("#dashboard_layout_swap_axis").off().click(function() {
        var axis_1 = $("#dashboard_layout_axis_1 span").html();
        var axis_2 = $("#dashboard_layout_axis_2 span").html();
        $("#dashboard_layout_axis_1 span").html(axis_2);
        $("#dashboard_layout_axis_2 span").html(axis_1);
        var opt = $("#dashboard_layout_plot_options").val();
        if (opt == "StackedBarChart") {
          var axis_2 = $("#dashboard_layout_axis_2 span").html();
          $("#dashboard_layout_plot_title").val(opt + " of " + axis_2);
        }
      });

      $("#dashboard_layout_plot_num_scrolls").off().change(function() {
        var val = $(this).val();
        if (val <= 0) {
          $(this).val(1);
        }
      });
      $("#dashboard_layout_add_plot").off().click(function() {
        // get plot dimensions
        var layout_plot_dimensions = $("#dashboard_layout_select_dimensions").val();
        // configuration
        var num_scrolls = Math.ceil(parseInt($("#dashboard_layout_plot_num_scrolls").val()) / 9999.0) - 1;
        var layout_plot_config = {
          num_scrolls: num_scrolls
        };
        // get plot option
        var layout_plot_option = $("#dashboard_layout_plot_options").val();
        if (layout_plot_dimensions.length == 2) {
          if (layout_plot_option.search("Categorical") < 0 && layout_plot_option.search("Geo") < 0) {
            layout_plot_dimensions = [];
            var layout_plot_dimension_1 = $("#dashboard_layout_axis_1 span").html();
            var layout_plot_dimension_2 = $("#dashboard_layout_axis_2 span").html();
            layout_plot_dimensions.push(layout_plot_dimension_1);
            if (layout_plot_dimension_2 != "") {
              layout_plot_dimensions.push(layout_plot_dimension_2);
            }
          }
        } else if (layout_plot_dimensions.length == 1) {
          // config cases date interval
          var id_type = global.dashboard.dimensions_types.find(x => x.id == layout_plot_dimensions[0]).type;
          if (id_type == "date") {
            layout_plot_config['interval'] = "month";
          }
        }
        // get plot title
        var layout_plot_title = $("#dashboard_layout_plot_title").val();
        // define layout plot object
        var layout_option = {
          title: layout_plot_title,
          timestamp: Date.now(),
          dimensions: layout_plot_dimensions,
          option: layout_plot_option,
          config: layout_plot_config
        };
        // only add if keys are not empty
        if (global.dashboard.layout_options.length < 6) {
          if (layout_option.title != "" && layout_option.dimensions.length > 0 && layout_option.option != null) {
            // push to global
            global.dashboard.layout_options.push(layout_option);
            console.log("Update Filter Options: " + global.dashboard.filter_options);
            // add to layout dropdown
            var new_li = '<li data-timestamp="' + layout_option.timestamp + '" data-title="' + layout_option.title + '"><a class="blue-grey-text text-lighten-2">' + layout_option.title + '<i class="material-icons orange-text">delete</i></a></li>';
            $("#data_dropdown_layout_options").append(new_li);
            // update number of filters
            $("#dropdown_layout_options span").html($("#data_dropdown_layout_options li").length);
            // new filter sign
            if (!$('#dropdown_layout_options span.new').length) {
              $('#dropdown_layout_options span').toggleClass("new");
            }
            console.log("Add Layout Option: " + JSON.stringify(layout_option));
            // clear dashboard layout content
            $(this).api.helper.dashboard_layout_clear();
            M.toast({
              html: 'Layout: ' + layout_plot_title + ' added',
              displayLength: 4000
            });
          }
        } else {
          M.toast({
            html: 'Layout: Maximum of 6 plots reached',
            displayLength: 4000
          });
        }
      });
    },
    // api
    plot_dashboard: function() {
      $(this).event.progress(true);
      $("#plot_dashboard_body").fadeTo(0, 1);
      // reset body divs
      $("#plot_dashboard_body").html('\
            <div class="plot_dashboard_body_tile" id="plot_dashboard_body_tile_0"></div>\
            <div class="plot_dashboard_body_tile" id="plot_dashboard_body_tile_1"></div>\
            <div class="plot_dashboard_body_tile" id="plot_dashboard_body_tile_2"></div>\
            <div class="plot_dashboard_body_tile" id="plot_dashboard_body_tile_3"></div>\
            <div class="plot_dashboard_body_tile" id="plot_dashboard_body_tile_4"></div>\
            <div class="plot_dashboard_body_tile" id="plot_dashboard_body_tile_5"></div>\
            ');
      $("#plot_dashboard_body").children().css({
        "width": 0,
        "height": 0
      }).fadeOut(0);
      var plot_dashboard_request = {
        layout: global.dashboard.layout_options,
        selected_dimensions: global.dashboard.selected_dimensions,
        filter: global.dashboard.filter_options
      }
      $.ajax({
        type: 'POST',
        url: "/_plot_dashboard",
        data: JSON.stringify(plot_dashboard_request),
        dataType: 'json',
        contentType: 'application/json',
        timeout: global.config.ajax_timeout
      }).done(function(data) {
        $(this).api.helper.plot_dashboard(data);
        $(this).event.dashboard_number_datasets();
      }).fail(function() {
        M.toast({
          html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
          displayLength: 4000
        });
        $(this).event.progress(false);
        console.log("Failed.");
      });
    }
  }
});
